<%@page import="appventas.bodega.Bodega"%>
<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Html.html to edit this template
-->
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> 
<link rel="stylesheet" type="text/css" href="css/estilo.css" media="screen" />
<script src="scripts/ajax.js"></script>
<script src="scripts/Fecha.js"></script>
<script src="scripts/ajax.js"></script>
<script src="scripts/MovimientoStock.js"></script>
<script>
   function cerrarModal(){
    $("#divproducto").modal('hide');
   }
</script>
    </head>
    <body>
         <%@include file="../include/navview.jsp" %>
      <div align="center">
        <h1>
        INFORME MOVIMIENTO POR PRODUCTO
        </h1>

<div class="container">
    <form class="navbar-form" action="" method="post">
        <fieldset>
                <legend>PRODUCTO SELECCIONADO:</legend>
               	<label for="ProductoCod">C�digo:</label>
                <input name="ProductoCod" id="ProductoCod">
                <button type="button" name="btnListadoProductos" id="btnListadoProductos" data-toggle="modal" data-target="#divproducto"    class="btn btn-primary btn-sm">
                <span class="glyphicon glyphicon-search"></span>Buscar 
                </button>
                <label for="ProductoNom">Producto:</label>
		<input name="ProductoNom" id="ProductoNom">
               <label for="Bodedga">Bodega:</label>
		
                <select id="BodegaId" name="BodegaId" class="style-select">
            <% 
             List<Bodega> arraylistbodega = (ArrayList<Bodega>)request.getSession(true).getAttribute("arraylistbodega");
                for (Bodega bodega: arraylistbodega){
           %>     
           <option value="<% out.print(bodega.getBodegaid());  %>"><% out.print(bodega.getBodeganom()); %></option>

                
                <% } %>
            </select>
         </fieldset>
        <br><br><br>
        <fieldset>          
		<legend>RANGO DE BUSQUEDA:</legend>
               	<label for="FechaDesde">Desde:</label>
                <input   type="date" name="FechaDesde" id="FechaDesde">
                <label for="FechaHasta">Hasta:</label>
		<input  type="date" name="FechaHasta" id="FechaHasta">
                Formato:
                <select id="TipoReporte" name="TipoReporte" class="style-select">
                    <option value="pdf">PDF</option>
                    <option value="xls">EXCEL</option>
                </select>
                    <button class="btn btn-primary btn-sm" type="Button" onclick="CargaReporte();">
                 <span class="glyphicon glyphicon-search"></span>
                    Buscar</button>
               <button class="btn btn-primary btn-sm" type="Button" onclick="window.location='movimientoprod';">
                 <span class="glyphicon glyphicon-file"></span>
                    Nuevo</button>
                
                <button class="btn btn-primary btn-sm" type="Button" onclick="window.location='index.jsp';">
                 <span class="glyphicon glyphicon-home"></span>
                    Home</button>  
        </fieldset>  
                
</form>
    <div id="contenido">
    </div>
</div>
              </div>
        
        
<%@include file="../productoview/divlistaproducto.jsp" %>
        
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    </body>
</html>
