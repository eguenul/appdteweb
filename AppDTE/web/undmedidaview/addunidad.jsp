<%-- 
    Document   : bodega
    Created on : 29-03-2023, 16:36:50
    Author     : esteban
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>UNIDADES DE MEDIDA</title>
      
<meta name="viewport" content="width=device-width, initial-scale=1"> <!?Con esto garantizamos que se vea bien en dispositivos móviles?> 
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> 
<link rel="stylesheet" type="text/css" href="css/estilo.css" media="screen" />
<script src="scripts/UndMedida.js"></script>    
<script src="scripts/ajax.js"></script>    
</head>
    <body>
        <%@include file="../include/navview.jsp" %>
        <div align="center">
        <h1>UNIDADES DE MEDIA</h1>
        </div>
        <div id="adminundmedida">
        <%@include file="../undmedidaview/formunidad.jsp" %>
        </div>
        
    <%@include file="../undmedidaview/listundmedida.jsp" %>
          
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
         </body>
</html>
