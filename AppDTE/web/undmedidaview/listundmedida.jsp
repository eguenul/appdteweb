<div id="divundmedida" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">LISTA UNIDADES DE MEDIDA</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

              
<table class="table">
       <thead>  
            
<tr>
    <th>ID</th>
    <th>DESCRIPCION</th>
    <th>NOMENCLATURA</th>
   
  </tr>
       </thead>
  <tr>
      
      <td><input id="idUndMedida2" name="idUndmMedida2"  onkeypress="return isNumberKey(event);" onkeyup="if(this.value.length>0){ cargarAjax('searchUndMedida','ACC=searchID&idUndMedida='+this.value,'contenido');}" ></td>
      <td><input id="UndMedidaDes2" name="UndMedidaDes2"  onkeypress="return isNumberKey(event);" onkeyup="if(this.value.length>0){ cargarAjax('searchUndMedida','ACC=searchDes&UndMedidaDes='+this.value,'contenido');}" ></td>
      <td><input id="UndMedidaNom2" name="UndMedidaNom2" onkeyup="if(this.value.length>0){ cargarAjax('searchUndMedida','ACC=searchNom&UndMedidaNom='+this.value,'contenido');}"><button onclick="idUndMedida2.value=''; UndMedidaDes2.value=''; UndMedidaNom2.value=''; cargarAjaxGet('listUndMedida','contenido');" type="button" name="btnRefresh" id="btnRefresh" class="btn btn-primary btn-sm">
            <span class="glyphicon glyphicon-refresh"></span> 
        </button>
    </td>
      
  </tr>
</table>    
<div id="listaundmedida">  
    <br>
    <div id="contenido">
             <%@include file="../undmedidaview/listundmedida2.jsp" %>
    </div>
</div>

    

<input type="hidden" id="pagina" name="pagina" value="">
</div>
</div>    
</div>
</div>    
