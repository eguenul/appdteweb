<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>POS - Punto de Venta</title>
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 20px;
    }
    form {
        margin-bottom: 20px;
    }
    label {
        display: block;
        margin-bottom: 5px;
    }
    
    button {
        padding: 5px 10px;
        background-color: #007bff;
        color: #fff;
        border: none;
        cursor: pointer;
    }
    button:hover {
        background-color: #0056b3;
    }
    table {
        border-collapse: collapse;
        width: 100%;
    }
    th, td {
        border: 1px solid #ddd;
        padding: 8px;
        text-align: left;
    }
    th {
        background-color: #f2f2f2;
    }
    #total-section {
        margin-top: 20px;
        border-top: 2px solid #ddd;
        padding-top: 10px;
    }
    #total-section h2 {
        margin-bottom: 5px;
    }
    #total-section span {
        font-weight: bold;
    }
</style>
</head>
<body>

<form action="#" method="GET">
    <table>
        <tr>
            <td><label for="clicod">C�digo del Cliente:</label></td>
            <td><input type="text" id="clicod" name="clicod" placeholder="Ingrese el c�digo del cliente" required></td>
            <td><label for="cliente">Nombre del Cliente:</label></td>
            <td><input type="text" id="cliente" name="cliente" placeholder="Ingrese el nombre del cliente" required></td>
            <td><button type="submit">Buscar Cliente</button></td>
        </tr>
    </table>
</form>

<table  class="table table-bordered" id="TablaDetalle">
    <tr>
        <td colspan="9" align="right">
            <button onclick="AgregaDetalle();" class="btn btn-primary btn-sm"  name="btnAgregar" type="button" id="btnAgregar">
                <span class="glyphicon glyphicon-plus"></span>Agregar</button>

            <button name="btnEliminar" class="btn btn-primary btn-sm" onclick="EliminaFila();" type="button" id="btnEliminar">
                <span class="glyphicon glyphicon-minus"></span>Eliminar</button>
        </td>
    </tr>
    <tr>
        <th>BAR CODE</th>
       
        <th>CODIGO</th>
        <th>DESCRIPCION PRODUCTO</th>
        <th>UNIDAD MEDIDA</th>
        <th>PRECIO</th>
        <th>%DESC</th>
        <th>CANTIDAD</th>
        <th>TOTAL</th>
        <th>&nbsp;</th>
        
    </tr>
    <tr>
        <td>BAR CODE</td>
        <td>
            <input  size="5" readonly="yes" maxlength="8" name="ProductoCod" type="text" id="ProductoCod">
            <button type="button" name="btnListadoProductos" id="btnListadoProductos" data-toggle="modal" data-target="#divproducto"    class="btn btn-primary btn-sm">
                <span class="glyphicon glyphicon-search"></span>Buscar 
            </button>
        </td>
        <td>
            <input size="15" readonly="yes" name="ProductoNom" type="text" id="ProductoNom">      
        </td>
        <td>
            <input readonly="yes" size="8"   name="UnidadMedidaNom" type="text" id="UnidadMedidaNom">      
        </td>
        <td>
            <input size="8" readonly="yes"  name="ProductoPre" type="text" id="ProductoPre">     
        </td>
        <td>
            <input onkeypress="return filterFloat(event,this);" onkeyup="if(Cantidad.value>0){ CalculaDescuento();}"     name="Porcentaje"  type="text" id="Porcentaje" value="0" size="4" maxlength="4">
        </td>
        <td>
            <input onkeypress="return filterFloat(event,this);" onkeyup="if(TipoMovimiento.value==1){ CalculaDescuento();}else{  Total.value = Math.floor((eval(this.value)* eval(ProductoPre.value)));}  if(this.value.length<1){ Total.value=0;}" size="8" name="Cantidad" type="text" id="Cantidad">   
        </td>
        <td>
            <input size="8" readonly="yes" name="Total" type="text" id="Total">     
            <input name="AfectoExento" type="hidden" id="AfectoExento">     
            <input name="PrecioOriginal" type="hidden" id="PrecioOriginal">     
        </td>
        <td>&nbsp;</td>
    </tr> 
     <tr>    
        <th colspan="9">
            DETALLE DOCUMENTO
        </th>
    </tr>
    <tr>
        
        <th>BAR CODE</th>
        <th>CODIGO</th>
        <th>DESCRIPCION PRODUCTO</th>
        <th>UNIDAD MEDIDA</th>
        <th>PRECIO</th>
        <th>%</th>
        <th>CANTIDAD</th>
        <th>TOTAL</th>
        <th>ACCION</th>
    </tr>
    
     <tr>
         <td>BAR CODE</td>
        <td>CODIGO</td>
        <td>DESCRIPCION PRODUCTO</td>
        <td>UNIDAD MEDIDA</td>
        <td>PRECIO</td>
        <td>%</td>
        <td>CANTIDAD</td>
        <td>TOTAL</td>
        <td><button>Eliminar</button></td>
    </tr>
    
</table>

<!-- Secci�n de Total de la Venta -->
<div id="total-section">
    <h2>Total de la Venta</h2>
    <span id="total-amount">$0.00</span>
</div>

</body>
</html>
