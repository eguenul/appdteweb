<div id="divbodega" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">LISTADO DE BODEGAS</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

              
<table class="table">
       <thead>  
            
<tr>
    <th>CODIGO</th>
    <th>NOMBRE</th>
   
  </tr>
       </thead>
  <tr>
      <td><input id="BodegaCod2" name="BodegaCod2"  onkeypress="return isNumberKey(event);" onkeyup="if(this.value.length>0){ cargarAjax('searchBodega','ACC=searchCod&BodegaCod='+this.value,'contenido');}" ></td>
      <td><input id="BodegaNom2" name="BodegaNom2" onkeyup="if(this.value.length>0){ cargarAjax('searchBodega','ACC=searchNom&BodegaNom='+this.value,'contenido');}"><button onclick="BodegaCod2.value=''; BodegaNom2.value=''; cargarAjaxGet('listBodega','contenido');" type="button" name="btnRefresh" id="btnRefresh" class="btn btn-primary btn-sm">
            <span class="glyphicon glyphicon-refresh"></span> 
        </button>
    </td>
      
  </tr>
</table>    
<div id="listabodega">  
    <br>
    <div id="contenido">
             <%@include file="../bodegaview/listbodega2.jsp" %>
    </div>
</div>

    

<input type="hidden" id="pagina" name="pagina" value="">
</div>
</div>    
</div>
</div>    