<nav class="navbar navbar-default" role="navigation">
  <div class="navbar-header">
    <!-- Se eliminó el contenido vacío -->
  </div>

  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="glyphicon glyphicon-cog"></span> MANTENCION<b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li><a href=empresa>EMPRESAS</a></li>
          <li><a href="producto">PRODUCTOS</a></li>
          <li><a href="cliprov">CLIENTE/PROVEEDOR</a></li>
          <li><a href="addBodega">BODEGAS</a></li>
          <li><a href="adminUndMedida">UNIDADES DE MEDIDA</a></li>
          <li><a href="usuario">USUARIOS</a></li>
          <li><a href="adminCAF">ADMINISTRACION CAF</a></li>
          <li><a href="adminCert">CARGA CERTIFICADO DIGITAL</a></li>
          <li><a href="setFirmaPass">CLAVE FIRMA ELECTRONICA</a></li>
          <li><a href="correlativo">CORRELATIVOS</a></li>
          <li><a href="setpass">CAMBIO PASSWORD ADMINISTRADOR</a></li>
        </ul>
      </li>
       <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="glyphicon glyphicon-tasks"></span>PUNTO DE EVENTA<b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li><a href="boletaservlet">EMISION BOLETA DE VENTA</a></li>
           </ul>
      </li>
      
      
      
      
      
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="glyphicon glyphicon-tasks"></span> PROCESOS <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li><a href="movimiento">EMISION DTE VENTA</a></li>
          <li><a href="dtecompra">EMISION DTE COMPRA</a></li>
          <li><a href="notapedido">EMISION DTE DESDE NOTA PEDIDO</a></li>
          <li><a href="adminStock">CARGA STOCK</a></li>
          <li><a href="Merma">REGISTRO DE MERMAS</a></li>
          <li><a href="selempresa">SELECCION EMPRESA</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="glyphicon glyphicon-print"></span> INFORMES <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li><a href="libroventas">LIBRO DE VENTAS</a></li>
          <li><a href="busquedafecha">HISTORIAL DTE</a></li>
          <li><a href="informestock">STOCK EN BODEGA</a></li>
          <li><a href="movimientoprod">INFORME MOVIMIENTO POR PRODUCTO</a></li>
          <li><a href="vtaproducto">INFORME VENTAS POR PRODUCTO</a></li>
          <li><a href="vtasCliProv">INFORME VENTAS POR CLIENTE</a></li>
        </ul>
      </li>
    </ul>

    <ul class="nav navbar-nav navbar-right">
      <li>
        <span style="color:white" class="navbar-text">
          RAZON SOCIAL: <% out.print(request.getSession().getAttribute("EmpresaRaz")); %>
        </span>
      </li>

      <li>
        <span style="color:white" class="navbar-text">
          RUT: <% out.print(request.getSession().getAttribute("EmpresaRut")); %>
        </span>
      </li>

      <li><a href="#">
        <span class="glyphicon glyphicon-user"></span>
        <% out.print(request.getSession().getAttribute("login")); %>
      </a></li>

      <li>
        <a href="logout.jsp">
          <span class="glyphicon glyphicon-log-out"></span> SALIR
        </a>
      </li>
    </ul>
  </div>
</nav>
