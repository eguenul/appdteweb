<%-- 
    Document   : index
    Created on : 16-oct-2018, 10:50:57
    Author     : esteban
--%>
<%@page import="appventas.empresa.Empresa"%>
<%
if(request.getSession().getAttribute("loginauth")==null){
         request.getRequestDispatcher("login").forward(request, response); 
}

if(request.getSession().getAttribute("empresaid")==null){
         request.getRequestDispatcher("selempresa").forward(request, response); 
}
Empresa objEmpresa = (Empresa) request.getSession().getAttribute("Empresa");


if(objEmpresa.getEmpresaid()==0){
    request.getRequestDispatcher("selempresa").forward(request, response); 
}
%>
 <%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>    
<meta charset="utf-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1"> <!?Con esto garantizamos que se vea bien en dispositivos móviles?> 
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> 
<link rel="stylesheet" type="text/css" href="css/estilo.css" media="screen" />
<title>APPDTE</title>
    </head>
    <body>
<%@include file="include/navview.jsp" %>
        <div align="center">
            <table>
                <tr>
                    <th colspan="2">
                        EMPRESA SELECCIONADA
                    </th>
                </tr>
                <tr>
                    <td>
                        RAZON SOCIAL
                    </td>
                    <td>
                        <% out.print(objEmpresa.getEmpresaraz()); %>
                    </td>               
                </tr>
                <tr>
                    <td>
                        RUT
                    </td>
                    <td>
                        <% out.print(objEmpresa.getEmpresarut()); %>
                    </td>               
                </tr>
                
            </table>
            
            
        </div>
                 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>
