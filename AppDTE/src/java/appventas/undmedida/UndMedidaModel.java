/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.undmedida;

import appventas.include.Conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class UndMedidaModel {
    
    
public void addUnidadMedida(UndMedida objUndMedida){
    try {
        
        String UndMedidaDes = objUndMedida.getUndmedidades();
        String UndMedidaNom = objUndMedida.getUndmedidanomen();
        
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        Statement stm = objConexion.getConexion().createStatement();
        String sql = "Insert UndMedida (UndMedidaDes,UndMedidaNom) values('"+ UndMedidaDes+"',"+"'"+UndMedidaNom+"')";
        stm.execute(sql);
        objConexion.cerrar();
        
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(UndMedidaModel.class.getName()).log(Level.SEVERE, null, ex);
    }
}   
        
    public ArrayList<UndMedida> listUndMedida(){
             
    try {
        ArrayList<UndMedida> arraylistundmedida;
        arraylistundmedida = new ArrayList<>();
       
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        Statement smt1 = objConexion.getConexion().createStatement();
        ResultSet objrecordset = smt1.executeQuery("Select * from UndMedida LIMIT 0,5");  
        
        while(objrecordset.next()){
            UndMedida objUndMedida = new UndMedida();
            objUndMedida.setUndmedidades(objrecordset.getString("UndMedidaDes"));
            objUndMedida.setUndmedidanomen(objrecordset.getString("UndMedidaNom"));
           objUndMedida.setIdundmedida(objrecordset.getInt("idUndMedida"));
            
            arraylistundmedida.add(objUndMedida);
        }
        
        objConexion.cerrar();
        return arraylistundmedida;
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(UndMedidaModel.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
       
       
   }  
       
    
    
    
  public ArrayList<UndMedida> searchUndMedidabyId(int idUndMedida) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
         ArrayList<UndMedida> arraylistundmedida;
        arraylistundmedida = new ArrayList<>();
       
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        Statement smt1 = objConexion.getConexion().createStatement();
        ResultSet objrecordset = smt1.executeQuery("Select * from UndMedida where idUndMedida="+String.valueOf(idUndMedida));  
        
        while(objrecordset.next()){
            UndMedida objUndMedida = new UndMedida();
            objUndMedida.setUndmedidades(objrecordset.getString("UndMedidaDes"));
            objUndMedida.setUndmedidanomen(objrecordset.getString("UndMedidaNom"));
           objUndMedida.setIdundmedida(objrecordset.getInt("idUndMedida"));
            
            arraylistundmedida.add(objUndMedida);
        }
        
        objConexion.cerrar();
        return arraylistundmedida;
    
         
     }
    
  
   public ArrayList<UndMedida> searchUndMedidabyDes(String UndMedidaDes) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
         ArrayList<UndMedida> arraylistundmedida;
        arraylistundmedida = new ArrayList<>();
       
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        Statement smt1 = objConexion.getConexion().createStatement();
        ResultSet objrecordset = smt1.executeQuery("Select * from UndMedida where UndMedidaDes like '" + UndMedidaDes+"%'");  
        
        while(objrecordset.next()){
            UndMedida objUndMedida = new UndMedida();
            objUndMedida.setUndmedidades(objrecordset.getString("UndMedidaDes"));
            objUndMedida.setUndmedidanomen(objrecordset.getString("UndMedidaNom"));
           objUndMedida.setIdundmedida(objrecordset.getInt("idUndMedida"));
            
            arraylistundmedida.add(objUndMedida);
        }
        
        objConexion.cerrar();
        return arraylistundmedida;
    
         
     }
  
   
   public ArrayList<UndMedida> searchUndMedidabyNom(String UndMedidaNom) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
         ArrayList<UndMedida> arraylistundmedida;
        arraylistundmedida = new ArrayList<>();
       
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        Statement smt1 = objConexion.getConexion().createStatement();
        ResultSet objrecordset = smt1.executeQuery("Select * from UndMedida where UndMedidaNom like '" + UndMedidaNom+"%'");  
        
        while(objrecordset.next()){
            UndMedida objUndMedida = new UndMedida();
            objUndMedida.setUndmedidades(objrecordset.getString("UndMedidaDes"));
            objUndMedida.setUndmedidanomen(objrecordset.getString("UndMedidaNom"));
           objUndMedida.setIdundmedida(objrecordset.getInt("idUndMedida"));
            
            arraylistundmedida.add(objUndMedida);
        }
        
        objConexion.cerrar();
        return arraylistundmedida;
    
         
     }
  
   
   
   public UndMedida getUndMedida(int idUndMedida) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
         
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        Statement smt1 = objConexion.getConexion().createStatement();
        ResultSet objrecordset = smt1.executeQuery("Select * from UndMedida where idUndMedida=" + String.valueOf(idUndMedida));  
        UndMedida objUndMedida = new UndMedida();
       
        while(objrecordset.next()){
            objUndMedida.setUndmedidades(objrecordset.getString("UndMedidaDes"));
            objUndMedida.setUndmedidanomen(objrecordset.getString("UndMedidaNom"));
           objUndMedida.setIdundmedida(objrecordset.getInt("idUndMedida"));
            
        }
        
        objConexion.cerrar();
       
        return objUndMedida; 
     }
  
   
   
   
   
   
   
   public void updateUndMedida(UndMedida objUndMedida){
    try {
        String UndMedidaDes = objUndMedida.getUndmedidades();
        String UndMedidaNom = objUndMedida.getUndmedidanomen();
        int idUndMedida = objUndMedida.getIdundmedida();
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        Statement stm = objConexion.getConexion().createStatement();
        String sql = "Update UndMedida set UndMedidaDes='"+UndMedidaDes+"', UndMedidaNom='"+UndMedidaNom + "' where idUndMedida="+ String.valueOf(idUndMedida);
        stm.execute(sql);
        objConexion.cerrar();
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(UndMedidaModel.class.getName()).log(Level.SEVERE, null, ex);
    }
        
       
       
   }
   
   
    public ArrayList<UndMedida> listallUndMedida(){
             
    try {
        ArrayList<UndMedida> arraylistundmedida;
        arraylistundmedida = new ArrayList<>();
       
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        Statement smt1 = objConexion.getConexion().createStatement();
        ResultSet objrecordset = smt1.executeQuery("Select * from UndMedida");  
        
        while(objrecordset.next()){
            UndMedida objUndMedida = new UndMedida();
            objUndMedida.setUndmedidades(objrecordset.getString("UndMedidaDes"));
            objUndMedida.setUndmedidanomen(objrecordset.getString("UndMedidaNom"));
           objUndMedida.setIdundmedida(objrecordset.getInt("idUndMedida"));
            
            arraylistundmedida.add(objUndMedida);
        }
        
        objConexion.cerrar();
        return arraylistundmedida;
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(UndMedidaModel.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
       
       
   }  
    public String getUndMedidaNom(int idUndMedida) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
         
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        Statement smt1 = objConexion.getConexion().createStatement();
        ResultSet objrecordset = smt1.executeQuery("Select * from UndMedida where idUndMedida=" + String.valueOf(idUndMedida));  
        String undmedidanom = new String();
        while(objrecordset.next()){
           undmedidanom = objrecordset.getString("UndMedidaNom");
            
        }
        
        objConexion.cerrar();
       
        return undmedidanom; 
     }
  
   
  
    
}
