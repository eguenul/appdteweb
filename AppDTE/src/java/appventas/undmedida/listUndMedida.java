package appventas.undmedida;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;






public class listUndMedida extends HttpServlet{
@Override
public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
    UndMedidaModel objUndMedidaModel = new UndMedidaModel();
    ArrayList<UndMedida> arraylistundmedida = objUndMedidaModel.listUndMedida();
    request.getSession().setAttribute("arraylistundmedida",arraylistundmedida);
    getServletConfig().getServletContext().getRequestDispatcher("/undmedidaview/listundmedida2.jsp").forward(request,response);
}
}
