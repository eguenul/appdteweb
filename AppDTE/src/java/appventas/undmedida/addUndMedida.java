/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.undmedida;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author esteban
 */
public class addUndMedida extends HttpServlet {
      
@Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
 
    
    
 String undmedidades = request.getParameter("UndMedidaDes");
 String undmedidanom = request.getParameter("UndMedidaNom");   
 
 UndMedida objUndMedida = new UndMedida();
 objUndMedida.setUndmedidades(undmedidades);
 objUndMedida.setUndmedidanomen(undmedidanom);
 UndMedidaModel objUndMedidaModel = new UndMedidaModel();
 
 
 String acc = request.getParameter("ACC");
 
 switch(acc){
 
     case "GRABAR":
            objUndMedidaModel.addUnidadMedida(objUndMedida);
            break;
            
          case "UPDATE":
            int idUndMedida = Integer.parseInt(request.getParameter("idUndMedida"));
            objUndMedida.setIdundmedida(idUndMedida);
            objUndMedidaModel.updateUndMedida(objUndMedida);
            break;
            
             
            
            
            
 }
request.getSession().setAttribute("idUndMedida", "");
request.getSession().setAttribute("UndMedidaNom", "");
request.getSession().setAttribute("UndMedidaDes", "");
 request.getSession().setAttribute("ACC","GRABAR");
 getServletConfig().getServletContext().getRequestDispatcher("/undmedidaview/formunidad.jsp").forward(request,response);
  
}

}
