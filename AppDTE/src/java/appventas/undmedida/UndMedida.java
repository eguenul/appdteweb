/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.undmedida;

/**
 *
 * @author esteban
 */
public class UndMedida {
    private String undmedidades;
    private String undmedidanomen;
    private int idundmedida;
    
    

    public String getUndmedidades() {
        return undmedidades;
    }

    public void setUndmedidades(String undmedidades) {
        this.undmedidades = undmedidades;
    }

    public String getUndmedidanomen() {
        return undmedidanomen;
    }

    public void setUndmedidanomen(String undmedidanomen) {
        this.undmedidanomen = undmedidanomen;
    }

    public int getIdundmedida() {
        return idundmedida;
    }

    public void setIdundmedida(int idundmedida) {
        this.idundmedida = idundmedida;
    }
    
    
    
}
