/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.undmedida;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class searchUndMedida extends HttpServlet {
    
@Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
        UndMedidaModel objUndMedidaModel = new UndMedidaModel();
        ArrayList<UndMedida> arraylistundmedida = new ArrayList<>();
        String acc = request.getParameter("ACC");
        
        switch(acc){  
            
            case "searchID":
                int idUndMedida = Integer.parseInt(request.getParameter("idUndMedida"));
                arraylistundmedida = objUndMedidaModel.searchUndMedidabyId(idUndMedida);
                request.getSession().setAttribute("arraylistundmedida", arraylistundmedida);
                break;
                
                
            case "searchDes":
               String UndMedidaDes = request.getParameter("UndMedidaDes");
               arraylistundmedida = objUndMedidaModel.searchUndMedidabyDes(UndMedidaDes);
               request.getSession().setAttribute("arraylistundmedida", arraylistundmedida);
                break;
                
                
            case "searchNom":
               String UndMedidaNom = request.getParameter("UndMedidaNom");
               arraylistundmedida = objUndMedidaModel.searchUndMedidabyDes(UndMedidaNom);
               request.getSession().setAttribute("arraylistundmedida", arraylistundmedida);
               break;
                
        }         
        request.getSession().setAttribute("arraylistundmedida",arraylistundmedida);
        getServletConfig().getServletContext().getRequestDispatcher("/undmedidaview/listundmedida2.jsp").forward(request,response);
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
        response.setContentType("text/html");

        //Objetemos el escritor hacia el Cliente
        PrintWriter out = response.getWriter();

        //ya podemos enviar al navegador
        out.println("<div class=\"alert alert-warning\" role=\"alert\">\n" +
"ERROR DE BUSQUEDA. \n" +
"</div>)");

        //cerramos el escritor
        out.close(); 
        
        
        
        Logger.getLogger(searchUndMedida.class.getName()).log(Level.SEVERE, null, ex);
    }
}
}
