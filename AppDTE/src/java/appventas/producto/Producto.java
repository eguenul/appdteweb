/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appventas.producto;

public class Producto {
   private int productoid;
   private int productocod;
   private String productonom;
   private int undmedidaid;
   private String undmedidanom;

   
    public int getProductoid() {
        return productoid;
    }

    public void setProductoid(int productoid) {
        this.productoid = productoid;
    }

    public int getProductocod() {
        return productocod;
    }

    public void setProductocod(int productocod) {
        this.productocod = productocod;
    }

    public String getProductonom() {
        return productonom;
    }

    public void setProductonom(String productonom) {
        this.productonom = productonom;
    }

   

    public int getProductostock() {
        return productostock;
    }

    public void setProductostock(int productostock) {
        this.productostock = productostock;
    }

    public int getProductoiva() {
        return productoiva;
    }

    public void setProductoiva(int productoiva) {
        this.productoiva = productoiva;
    }
 private   int preciocosto;
 private   int precioventa;
 private   int productostock;
 private   int productoiva;

    public int getUndmedidaid() {
        return undmedidaid;
    }

    public void setUndmedidaid(int undmedidaid) {
        this.undmedidaid = undmedidaid;
    }

    public String getUndmedidanom() {
        return undmedidanom;
    }

    public void setUndmedidanom(String undmedidanom) {
        this.undmedidanom = undmedidanom;
    }

    public int getPreciocosto() {
        return preciocosto;
    }

    public void setPreciocosto(int preciocosto) {
        this.preciocosto = preciocosto;
    }

    public int getPrecioventa() {
        return precioventa;
    }

    public void setPrecioventa(int precioventa) {
        this.precioventa = precioventa;
    }
}
