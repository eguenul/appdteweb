package appventas.producto;
import appventas.include.Conexion;
import appventas.undmedida.UndMedidaModel;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class ProductoModel {
   
    private final int empresaid;
     
public ProductoModel(int empresaid){
    this.empresaid = empresaid;
    
}
    

public ArrayList<Producto> listProducto(int indice) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            
            String sql;
            sql = "Select * from Producto where EmpresaId="+String.valueOf(empresaid)+ " limit "+String.valueOf(indice)+",10";
            Statement stm = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stm.executeQuery(sql);
            ArrayList<Producto> arrayProducto = new ArrayList();
            
            while(objrecordset.next()){
                Producto objProducto = new Producto();
                objProducto.setProductocod(objrecordset.getInt("ProductoCod"));
                objProducto.setProductonom(objrecordset.getString("ProductoNom"));
                objProducto.setPrecioventa(objrecordset.getInt("PrecioVenta"));
                objProducto.setPreciocosto(objrecordset.getInt("PrecioCosto"));
                
                objProducto.setProductoiva(objrecordset.getInt("AfectoExento"));
                
                objProducto.setUndmedidaid(objrecordset.getInt("UndMedidaId"));
                       
                UndMedidaModel objUndMedidaModel = new UndMedidaModel();
                objProducto.setUndmedidanom(objUndMedidaModel.getUndMedidaNom(objProducto.getUndmedidaid()));
             
         
                arrayProducto.add(objProducto);
            }
            objConexion.cerrar();
            return arrayProducto;
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(ProductoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

}

public Producto searchProducto(int productocod) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            String sql;
            sql = "Select * from Producto where ProductoCod="+String.valueOf(productocod) +" and EmpresaId="+String.valueOf(empresaid);
            Producto objProducto = new Producto();
            Statement stm = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stm.executeQuery(sql);
            
            while(objrecordset.next()){
                objProducto.setProductocod(objrecordset.getInt("ProductoCod"));
                objProducto.setProductonom(objrecordset.getString("ProductoNom"));
                objProducto.setPrecioventa(objrecordset.getInt("PrecioVenta"));
                objProducto.setProductoiva(objrecordset.getInt("AfectoExento"));
                objProducto.setProductoid(objrecordset.getInt("ProductoId"));
                objProducto.setProductostock(objrecordset.getInt("ProductoStock"));
                objProducto.setUndmedidaid(objrecordset.getInt("UndMedidaId"));
                objProducto.setPreciocosto(objrecordset.getInt("PrecioCosto"));
                   
                UndMedidaModel objUndMedidaModel = new UndMedidaModel();
                objProducto.setUndmedidanom(objUndMedidaModel.getUndMedidaNom(objProducto.getUndmedidaid()));
             
         
            }
            objConexion.cerrar();
            return objProducto;
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(ProductoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
}
 
public int flagIva(int productocod) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            
            String sql;
            sql = "Select AfectoExento from Producto where ProductoCod="+String.valueOf(productocod)+" and EmpresaId="+String.valueOf(empresaid);
            System.out.print(sql);
            Statement stm = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stm.executeQuery(sql);
            int flag = 0;
            objrecordset.next();
            flag = objrecordset.getInt("AfectoExento");
            objConexion.cerrar();
            return flag;
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(ProductoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
}

public void addProducto(Producto objProducto) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            String sql;
            int productocod = objProducto.getProductocod();
            String productonom = objProducto.getProductonom();
            int productoprecioventa = objProducto.getPrecioventa();
            int productoiva = objProducto.getProductoiva();
            int undmedidaid = objProducto.getUndmedidaid();
            sql = "INSERT INTO Producto (ProductoCod,ProductoNom,PrecioVenta,AfectoExento,EmpresaId, UndMedidaId) \n"+
                    "values("+String.valueOf(productocod)+",'"+productonom+"',"+ String.valueOf(productoprecioventa)+","+String.valueOf(productoiva)+ ","+String.valueOf(empresaid)+ ","+ String.valueOf(undmedidaid)+ ")";
            Statement stm = objConexion.getConexion().createStatement();
            stm.execute(sql);
            updateCorrelativo();
            objConexion.cerrar();
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(ProductoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
}



public ArrayList<Producto> searchCod(int productocod) throws SQLException, ClassNotFoundException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            
            ArrayList<Producto> arraylista = new ArrayList<>();
            String sql ="Select * from Producto where ProductoCod="+String.valueOf(productocod)+" and EmpresaId="+String.valueOf(empresaid);
            Statement stmt = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stmt.executeQuery(sql);
            
            while(objrecordset.next()){
                Producto objProducto = new Producto();
                objProducto.setProductocod(objrecordset.getInt("ProductoCod"));
                objProducto.setProductonom(objrecordset.getString("ProductoNom"));
                objProducto.setPrecioventa(objrecordset.getInt("PrecioVenta"));
                objProducto.setProductoiva(objrecordset.getInt("AfectoExento"));
                objProducto.setProductoid(objrecordset.getInt("ProductoId"));
                objProducto.setUndmedidaid(objrecordset.getInt("UndMedidaId"));
                 objProducto.setPreciocosto(objrecordset.getInt("PrecioCosto"));
               
                UndMedidaModel objUndMedidaModel = new UndMedidaModel();
                objProducto.setUndmedidanom(objUndMedidaModel.getUndMedidaNom(objProducto.getUndmedidaid()));
             
                /*
                objProducto.setCliprovema(objrecordset.getString("CliProvEma"));
                objProducto.setCliprovraz(objrecordset.getString("CliProvRaz"));
                objProducto.setCliprovfon(objrecordset.getString("CliProvFon"));
                objProducto.setCliprovgir(objrecordset.getString("CliProvGir"));
                objProducto.setCliprovrut(objrecordset.getString("CliProvRut"));
                */
                arraylista.add(objProducto);
            }
            objConexion.cerrar();
            return arraylista;
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(ProductoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
}


public ArrayList<Producto> searchNom(String productonom) throws SQLException, ClassNotFoundException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            
            
            ArrayList<Producto> arraylista = new ArrayList<>();
            String sql ="Select * from Producto where ProductoNom LIKE '"+productonom+"%'"+" and EmpresaId="+String.valueOf(empresaid);
            Statement stmt = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stmt.executeQuery(sql);
            while(objrecordset.next()){
                Producto objProducto = new Producto();
                objProducto.setProductocod(objrecordset.getInt("ProductoCod"));
                objProducto.setProductonom(objrecordset.getString("ProductoNom"));
                objProducto.setPrecioventa(objrecordset.getInt("PrecioVenta"));
                objProducto.setProductoiva(objrecordset.getInt("AfectoExento"));
                objProducto.setProductoid(objrecordset.getInt("ProductoId"));
                objProducto.setUndmedidaid(objrecordset.getInt("UndMedidaId"));
            objProducto.setPreciocosto(objrecordset.getInt("PrecioCosto"));
               
                UndMedidaModel objUndMedidaModel = new UndMedidaModel();
                objProducto.setUndmedidanom(objUndMedidaModel.getUndMedidaNom(objProducto.getUndmedidaid()));
                
                /*
                objProducto.setCliprovema(objrecordset.getString("CliProvEma"));
                objProducto.setCliprovraz(objrecordset.getString("CliProvRaz"));
                objProducto.setCliprovfon(objrecordset.getString("CliProvFon"));
                objProducto.setCliprovgir(objrecordset.getString("CliProvGir"));
                objProducto.setCliprovrut(objrecordset.getString("CliProvRut"));
                */
                arraylista.add(objProducto);
                
                
                
            }
            objConexion.cerrar();
            return arraylista;
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(ProductoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
}


public void updateProducto(Producto objProducto) throws SQLException{
    
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            
            String sql;
            int productocod = objProducto.getProductocod();
            String productonom = objProducto.getProductonom();
            int productoprecioventa = objProducto.getPrecioventa();
            int productoiva = objProducto.getProductoiva();
            int undmedidaid = objProducto.getUndmedidaid();
            int preciocosto = objProducto.getPreciocosto();
            sql = "Update Producto  \n"+
                    "SET ProductoCod="+String.valueOf(productocod)+",\n"+
                    "ProductoNom='"+productonom+"', \n"+
                    "PrecioVenta="+productoprecioventa+",\n"+
                    "PrecioCosto="+preciocosto+",\n"+
                    
                    "AfectoExento="+productoiva+",\n"+
                    "UndMedidaId="+undmedidaid+"\n"+
                    
                    "where ProductoCod="+String.valueOf(productocod);
            
            Statement stm = objConexion.getConexion().createStatement();
            stm.execute(sql);
            objConexion.cerrar();
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(ProductoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    
   
    
    
   
}





 public int getProductoCod() throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            Statement stm = objConexion.getConexion().createStatement();
            String sql = "Select ProductoCod from Correlativo where EmpresaId="+ String.valueOf(this.empresaid);
            ResultSet objrecordset = stm.executeQuery(sql);
            int productocod = 0;
            
            while(objrecordset.next()){
                productocod = objrecordset.getInt("ProductoCod");
            }
            objConexion.cerrar();
            return productocod;
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(ProductoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

 
 private void updateCorrelativo() throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            Statement stm = objConexion.getConexion().createStatement();
            String sql = "Update Correlativo set ProductoCod=ProductoCod+1 where EmpresaId="+ String.valueOf(this.empresaid);
            stm.execute(sql);
            objConexion.cerrar();
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(ProductoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
     
     
 }















}
