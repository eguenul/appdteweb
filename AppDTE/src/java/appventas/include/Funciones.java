/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appventas.include;

import appventas.documento.DocumentoModel;
import appventas.empresa.Empresa;
import appventas.empresa.EmpresaModel;
import com.appdte.sii.utilidades.ConfigAppDTE;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class Funciones {
     public boolean buscaFolios(String login,int empresaid, String codsii) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
    
        
        String sql;
        sql="Select EmpresaRut from Empresa where EmpresaId="+String.valueOf(empresaid);
        
        ConfigAppDTE objconfig = new ConfigAppDTE();
        Conexion obj= new Conexion();
        obj.Conectar();
        Statement stm = obj.getConexion().createStatement();
        String pathcaf = objconfig.getPathcaf();
       
        ResultSet objrecordset = stm.executeQuery(sql);
        objrecordset.next();
        String empresarut = objrecordset.getString("EmpresaRut");
        
        String[] parts = empresarut.split("-");
        String rut = parts[0];
        
        String nombrefolio = pathcaf.trim()+login+"F"+rut.trim()+"T"+codsii+".xml";
        
        obj.cerrar();
       File archivo = new File(nombrefolio.trim());
       if (!archivo.exists()) {
          System.out.println("ERROR: ¡¡No existe el archivo de folios!!");
          return false;
      }
        return true;  
    }
     
     
     public void deleteCAF(int idCAF) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
         
        Conexion obj= new Conexion();
        obj.Conectar();
        Statement stm = obj.getConexion().createStatement();
        String sql="Delete from CAF where idCAF="+String.valueOf(idCAF);
        stm.execute(sql);
        obj.cerrar();
     }
     

     
public void addCertificado(String login,String filepath2) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
    
    
    
    Conexion objconexion = new Conexion();
     objconexion.Conectar();
    
     
  File file = new File(filepath2);
  byte[] bytes = Files.readAllBytes(file.toPath());
  
     
     
     
     
 
    String query = "Update Usuario set  BlobCert=?, NombreArchivo=? where UsuarioLogin='" + login + "'";
    
    PreparedStatement pstmt = objconexion.getConexion().prepareStatement(query);
    pstmt.setBytes(1, bytes);
    pstmt.setString(2, login+".pfx");
    pstmt.execute();
    objconexion.cerrar();

/*/    
    File fichero = new File(filepath2);   
    if (fichero.delete())
   System.out.println("El fichero ha sido borrado satisfactoriamente");
else
   System.out.println("El fichero no puede ser borrado");
   */ 
 

         
         
 }


public void loadCert(String login) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
    
         
             String sql = "SELECT * from Usuario where UsuarioLogin='"+ login + "'";
             System.out.print(sql);
             
             
             Conexion objconexion = new Conexion();
             objconexion.Conectar();
             
                FileOutputStream output = null;
             
             Statement  stmt = objconexion.getConexion().createStatement();
             ResultSet rs = stmt.executeQuery(sql);
                ConfigAppDTE objconfig = new ConfigAppDTE();
             if (rs.next()) {
                 File file = new File(objconfig.getPathcert()+rs.getString("NombreArchivo"));
                 output = new FileOutputStream(file);
                 
                 System.out.println("Leyendo archivo desde la base de datos...");
                 byte[] buffer = rs.getBytes("BlobCert");
                 
                 output.write(buffer);
                 
                 System.out.println("> Archivo guardado en : " + file.getAbsolutePath());
                 
             }
             
        
}

public int validaBlobCert(String login){
    
         try {
             String sql = "SELECT count(*) as CONTEO from Usuario where UsuarioLogin='"+ login + "' and BlobCert is not NULL";
             Conexion objconexion = new Conexion();
             objconexion.Conectar();
             int conteo = 0;
             Statement  stmt = objconexion.getConexion().createStatement();
             ResultSet rs = stmt.executeQuery(sql);
             while(rs.next()){
                 
                 conteo = rs.getInt("CONTEO");
             }
             System.out.print(conteo);
             return conteo;
         } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
             Logger.getLogger(Funciones.class.getName()).log(Level.SEVERE, null, ex);
         }
         return 0;

}


public void setClaveFirma(String login, String clavefirma) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
         
         
             String sql = "Update Usuario set ClaveFirma='"+clavefirma +"' where UsuarioLogin='"+ login + "'";
             System.out.print(sql);
             
             
             Conexion objconexion = new Conexion();
             objconexion.Conectar();
             
        
             
             Statement  stmt = objconexion.getConexion().createStatement();
              stmt.execute(sql);
         objconexion.cerrar();
}
     
public void loadCAF(int empresaid, int idtipodoc, String login) throws ParserConfigurationException, SAXException, IOException, SQLException, ClassNotFoundException{
    EmpresaModel objEmpresaModel = new EmpresaModel();
    Empresa objEmpresa = objEmpresaModel.getData(empresaid);
    String[] arrayrutempresa = objEmpresa.getEmpresarut().split("-");
    DocumentoModel objTipoDocumento = new DocumentoModel();
    
    String sql = "SELECT * from CAF where EmpresaId="+ String.valueOf(empresaid) + " and TipoCaf="+String.valueOf(idtipodoc);
    System.out.print(sql);
             
             
    Conexion objconexion = new Conexion();
    objconexion.Conectar();
             
    FileOutputStream output = null;
             
    Statement  stmt = objconexion.getConexion().createStatement();
    ResultSet rs = stmt.executeQuery(sql);
         
 
    ConfigAppDTE objConfig = new ConfigAppDTE();
    String pathcaf = objConfig.getPathcaf()+login+"F"+arrayrutempresa[0]+"T"+String.valueOf(objTipoDocumento.getSiiCod(idtipodoc))+".xml";
    
 
      if (rs.next()) {
                 File file = new File(pathcaf);
                 output = new FileOutputStream(file);
                 
                 System.out.println("Leyendo archivo desde la base de datos...");
                 byte[] buffer = rs.getBytes("BlobCAF");
                 
                 output.write(buffer);
                 
                 System.out.println("> Archivo guardado en : " + file.getAbsolutePath());
                 
             }
        
    objconexion.cerrar();

}     
     
     
     
     
}
