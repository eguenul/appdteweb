/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.include;

import appventas.cliprov.CliProv;
import appventas.empresa.Empresa;
import appventas.empresa.EmpresaModel;
import appventas.movimientos.Movimiento;
import appventas.movimientos.MovimientoModel;
import com.appdte.json.DetalleDteJson;
import com.appdte.sii.utilidades.ConfigAppDTE;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author esteban
 */
public class PrintStock {
 
    	
public void printPDF(int idmovimiento, int empresaid, String cabecera) throws Exception {
    
      MovimientoModel objMovimientoModel = new MovimientoModel();
     Object[] arraymovimiento = objMovimientoModel.showDocument(idmovimiento);
   
    EmpresaModel objEmpresaModel = new EmpresaModel();    
    Empresa objEmpresa = objEmpresaModel.getData(empresaid);
    
    
       CliProv objCliProv  = new CliProv();

        objCliProv.setCliprovrut((String)arraymovimiento[1]);
        objCliProv.setCliprovraz((String)arraymovimiento[2]);
        objCliProv.setCliprovgir((String)arraymovimiento[3]);
        objCliProv.setCliprovdir((String)arraymovimiento[4]);
        objCliProv.setCliprovcom((String)arraymovimiento[5]);
        objCliProv.setCliprovciu((String)arraymovimiento[6]);
       
    String fechamovimiento = (String) arraymovimiento[10];
    
    
   /* totales de movimiento */
       Movimiento objMovimiento = new Movimiento(); 
       objMovimiento.setMontoafecto((int)arraymovimiento[11]);
       objMovimiento.setMontoexento((int)arraymovimiento[12]);
       objMovimiento.setMontoiva((int)arraymovimiento[13]);
       objMovimiento.setMontototal((int)arraymovimiento[14]);
       
    int auxlinea = 1;

   
       
    

       ConfigAppDTE objConfig = new ConfigAppDTE();
       DecimalFormat formatea = new DecimalFormat("###,###.##");
       DecimalFormat formatea2 = new DecimalFormat("#,##0.00");
    
    
        /* cargo el template pdf */
        PdfReader reader = new PdfReader(objConfig.getPathtemplate()+ "templatenota.pdf");
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(objConfig.getPathpdf()+"CARGASTOCK.pdf"));
        PdfContentByte content = stamper.getOverContent(1);
        BaseFont bf=BaseFont.createFont(BaseFont.HELVETICA,BaseFont.CP1252,BaseFont.NOT_EMBEDDED);
        BaseFont bf2=BaseFont.createFont(BaseFont.HELVETICA_BOLD,BaseFont.CP1252,BaseFont.NOT_EMBEDDED);
        content.setFontAndSize(bf2, 14);
         /* ahora imprimo el rut y el folio del recuadro */
        content.setTextMatrix(470, 739);
        
        String arrayrutemisor[] =objEmpresa.getEmpresarut().split("-");
        
        String auxrutemisor = formatea.format(Integer.parseInt(arrayrutemisor[0]));
        String auxdvemisor = arrayrutemisor[1];
        
        content.showText(auxrutemisor+"-"+auxdvemisor);
         
       int y1 = 720;
        content.setTextMatrix(410, y1);
      
         
                        
                      content.showText(cabecera);
                      y1 = y1 - 20;
                      content.setTextMatrix(420, y1); 
                        
                      
            
        
        
        y1 = y1 - 20;
        
        int intvalue = 186;
        char convertedchar = (char)intvalue;
        content.setTextMatrix(445, y1);
        content.showText("N"+convertedchar);
        
        
        content.setTextMatrix(465, y1);
        content.showText(String.valueOf((int)arraymovimiento[9]));
        
        content.setFontAndSize(bf2, 13);
        content.setTextMatrix(395,654); 
        content.showText("SUCURSAL: " + objEmpresa.getEmpresacom());
        
        
        /* DATOS DEL EMISOR Y EMISION  */
        content.setFontAndSize(bf, 8);
        content.setTextMatrix(24,641); 
        content.showText(objEmpresa.getEmpresaraz());
        
        content.setTextMatrix(54,624); 
        content.showText(objEmpresa.getEmpresagir());
        
        content.setTextMatrix(92,604); 
        content.showText(objEmpresa.getEmpresadir());
        
        content.setTextMatrix(262,604); 
        content.showText(objEmpresa.getEmpresaciu());
        
        
        
        
        
        
        
        /* DATOS DEL RECEPTOR */
        
        content.setTextMatrix(429,576); 
        content.showText(objCliProv.getCliprovrut());
        content.setTextMatrix(429,556); 
        content.showText(objCliProv.getCliprovciu());
        content.setTextMatrix(429,536); 
        content.showText("-");
        content.setTextMatrix(429,516); 
      
        String dateString = fechamovimiento;
        Date parsed = new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        content.showText(df.format(parsed));
        
        content.setTextMatrix(109,576); 
        content.showText(objCliProv.getCliprovciu());
        
        content.setTextMatrix(109,556); 
        content.showText(objCliProv.getCliprovdir());
        
        content.setTextMatrix(109,536); 
        content.showText(objCliProv.getCliprovcom());
        
        content.setTextMatrix(109,516); 
        content.showText(objCliProv.getCliprovgir());
     
        /* cargo DETALLE DEL DOCUMENTO */
       
        
        
        int y = 450;
        
        
        
         
   
       
       
          
       
       
              /* cargo detallee */
              
               
    ArrayList<Object[]> arraydetalle = objMovimientoModel.showDetails(idmovimiento);
   
    for(Object[] i: arraydetalle){
    
         DetalleDteJson objDetalleJson = new DetalleDteJson();
         objDetalleJson.setNrolinea(auxlinea);
         objDetalleJson.setVlrcodigo(String.valueOf(i[0]));
         objDetalleJson.setNmbitem((String) i[1]);
         objDetalleJson.setQtyitem(formatea.format((BigDecimal) i[2]));
         objDetalleJson.setPrcitem((int) i[7]);
         objDetalleJson.setDescuentomonto(0);
         objDetalleJson.setMontoitem((int) i[6]);
   
       
       
          
       
       
              
                  
                  content.setFontAndSize(bf,10);
                  content.setTextMatrix(30,y);                   
                   content.showText(objDetalleJson.getVlrcodigo());
                    
                    content.setFontAndSize(bf,10);
                    content.setTextMatrix(125,y); 
                    content.showText(objDetalleJson.getNmbitem());
                    
                    
                    content.setFontAndSize(bf,10);
                    content.setTextMatrix(260,y); 
                    content.showText(formatea2.format((BigDecimal) i[2]));
           
            
                    content.setFontAndSize(bf,10);
                    content.setTextMatrix(353,y); 
                    content.showText(formatea.format(objDetalleJson.getPrcitem()));
                       
                    content.setFontAndSize(bf,10);
                    content.setTextMatrix(445,y); 
                    content.showText(formatea.format(objDetalleJson.getDescuentopct()));                
                   
                   
                    
                    
                    
                    
                    
                    content.setFontAndSize(bf,10);
                    content.setTextMatrix(510,y); 
                    content.showText(formatea.format(objDetalleJson.getMontoitem())); 
                    y = y - 20;
                    
                
                }
            
              
              
           
                     
        
        /* Imprimo totales del documento */
       content.setFontAndSize(bf,10);
       content.setTextMatrix(480,90); 
       content.showText(formatea.format(objMovimiento.getMontoafecto()));
       
       content.setFontAndSize(bf,10);
       content.setTextMatrix(480,70); 
       content.showText(formatea.format(objMovimiento.getMontoexento()));
     
     
       content.setFontAndSize(bf,10);
       content.setTextMatrix(480,50); 
       content.showText("19");
      
       
       content.setFontAndSize(bf,10);
       content.setTextMatrix(480,30); 
       content.showText(formatea.format(objMovimiento.getMontototal()));
        
       /*
       content.setFontAndSize(bf,10);
       content.setTextMatrix(189,51);
       content.showText(textores); 
       */
       
       content.setFontAndSize(bf,10);
       content.setTextMatrix(50,10);
       content.showText("DOCUMENTO NO CONSTITUYE OPERACION DE VENTA"); 
   
       
       
       
      
        stamper.close();
        
        
        
     
        
        
        
    
        
        
    }
    
    
    
       
}
