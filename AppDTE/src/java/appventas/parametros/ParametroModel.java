/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appventas.parametros;

import appventas.include.Conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class ParametroModel {
  

public ParametroModel(){
    
}

public float getIva() throws SQLException{
    try {
        Conexion objconexion = new Conexion();
        objconexion.Conectar();
        
        String sql;
        sql = "Select Iva from Parametros ";
        Statement stm = objconexion.getConexion().createStatement();
        ResultSet objrecordset = stm.executeQuery(sql);
        objrecordset.next();
        float iva = objrecordset.getFloat("Iva");
        objconexion.cerrar();
        return iva;
    } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(ParametroModel.class.getName()).log(Level.SEVERE, null, ex);
    }
    return 0;
    
}

public int getCodIva() throws SQLException{
    try {
        Conexion objconexion = new Conexion();
        objconexion.Conectar();
        
        String sql;
        sql = "Select CodIva from Parametros ";
        Statement stm = objconexion.getConexion().createStatement();
        ResultSet objrecordset = stm.executeQuery(sql);
        objrecordset.next();
        int codiva = objrecordset.getInt("CodIva");
        objconexion.cerrar();
        return codiva;
    } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(ParametroModel.class.getName()).log(Level.SEVERE, null, ex);
    }
    return 0;
    
}







}
