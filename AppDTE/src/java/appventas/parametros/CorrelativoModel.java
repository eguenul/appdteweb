/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appventas.parametros;

import appventas.include.Conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class CorrelativoModel {
      
public Correlativo showCorrelativo(int empresaid) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
        Correlativo objCorrelativo = new Correlativo();
        
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
       
        
        
        
  String sql = "Select * from Correlativo where EmpresaId=" + String.valueOf(empresaid);
    Statement stm = objConexion.getConexion().createStatement();
    ResultSet objrecordset = stm.executeQuery(sql);
    while( objrecordset.next()){
        
        objCorrelativo.setFacventaafecta(objrecordset.getInt("FacVentaAfecta"));
        objCorrelativo.setNotacredito(objrecordset.getInt("NotaCredito"));
        objCorrelativo.setFacventaexenta(objrecordset.getInt("FacVentaExenta"));
        objCorrelativo.setBoletaexentae(objrecordset.getInt("BoletaExenta"));
        objCorrelativo.setBoletaafectae(objrecordset.getInt("BolAfectaE"));
        objCorrelativo.setGuiadespachoe(objrecordset.getInt("GuiaDespacho"));
        
        
    }
    objConexion.cerrar();
        return objCorrelativo;
    }

public  void setCorrelativos(Correlativo objCorrelativo, int empresaid) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
       Conexion objConexion = new Conexion();
        objConexion.Conectar();
    String sql = "Update Correlativo set FacVentaAfecta=" + String.valueOf(objCorrelativo.getFacventaafecta()) + ",\n"+
                  "NotaCredito=" + String.valueOf(objCorrelativo.getNotacredito()) + ",\n"+
                  "FacVentaExenta=" + String.valueOf(objCorrelativo.getFacventaexenta()) + ",\n"+
                  "BoletaExenta=" + String.valueOf(objCorrelativo.getBoletaexentae()) + ",\n"+
                  "BolAfectaE=" + String.valueOf(objCorrelativo.getBoletaafectae()) + ",\n"+
                  "GuiaDespacho=" + String.valueOf(objCorrelativo.getGuiadespachoe()) + "\n"+
                  "Where EmpresaId=" + String.valueOf(empresaid);
    Statement stm = objConexion.getConexion().createStatement();
    stm.execute(sql);
    
    objConexion.cerrar();
}





}
