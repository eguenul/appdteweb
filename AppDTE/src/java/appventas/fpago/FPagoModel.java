/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.fpago;

import appventas.include.Conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class FPagoModel {
    public FPagoModel(){
        
    }
    
    
    
public ArrayList<FPago> listFpago() throws SQLException {
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            ArrayList<FPago> arraylist1 = new  ArrayList<>();
            String sql = "Select * from FPago";
            Statement stm = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stm.executeQuery(sql);
            while(objrecordset.next()){
                FPago objFPago = new FPago();
                objFPago.setIdfpago(objrecordset.getInt("idFPago"));
                objFPago.setFpagodes(objrecordset.getString("FPagoDes"));
                objFPago.setFpagosii(objrecordset.getString("FPagoSII"));
                arraylist1.add(objFPago);
            }
             objConexion.cerrar();
            return arraylist1;
           
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(FPagoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    
    
}


public int getFPagoId(int parmcodsii) throws SQLException {
        
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            int fpagoid = 0;
            String sql = "Select * from FPago where FPagoSII="+String.valueOf(parmcodsii);
            Statement stm = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stm.executeQuery(sql);
            while(objrecordset.next()){
                
                fpagoid = objrecordset.getInt("idFPago");
            }
            objConexion.cerrar();
            return fpagoid;
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(FPagoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
           
        
    
    
}











}
