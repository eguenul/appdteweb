/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.movimientos;

import appventas.tpoventa.TpoVenta;
import appventas.tpoventa.TpoVentaModel;
import appventas.bodega.Bodega;
import appventas.bodega.BodegaModel;
import appventas.cliprov.CliProv;
import appventas.cliprov.CliProvModel;
import appventas.documento.Documento;
import appventas.documento.DocumentoModel;
import appventas.fpago.FPago;
import appventas.fpago.FPagoModel;
import appventas.producto.Producto;
import appventas.producto.ProductoModel;
import appventas.referencia.Referencia;
import appventas.referencia.ReferenciaModel;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class adminStock extends HttpServlet{
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     if(request.getSession().getAttribute("loginauth")==null){
         request.getRequestDispatcher("login").forward(request, response); 
       }
     request.getSession().setAttribute("coddocsii", null);  
        
        
        int empresaid =  (int) request.getSession().getAttribute("empresaid");
        request.getSession().setAttribute("empresaid", empresaid);
        try {
           
         
        request.getSession().setAttribute("referencia","no");
        
        request.getSession().setAttribute("ReferenciaFlag",0);
        request.getSession().setAttribute("MovimientoId",0);
       
         BodegaModel objBodegaModel = new BodegaModel(empresaid);
           ArrayList<Bodega> arraylistbodega = objBodegaModel.listallBodegas();
           request.getSession().setAttribute("arraylistbodega", arraylistbodega);
       
        
        
        
       DespachoModel objDespacho = new DespachoModel();    
       List<Despacho> arraydespacho = objDespacho.listDespacho();
          
       
       TrasladoModel objTraslado = new TrasladoModel();    
       List<Traslado> arraytraslado = objTraslado.listTraslado();
    
       
       
      DocumentoModel objDocumentoModel = new DocumentoModel();
      List<Documento> arraylistdocumento = objDocumentoModel.listDocuments();
      
      CliProvModel objCliProvModel = new CliProvModel(empresaid);
      ArrayList<CliProv> arraylistcliprov = objCliProvModel.listaCliProv(0);
      
      ProductoModel objProductoModel = new ProductoModel(empresaid);
      ArrayList<Producto>  arrayproducto = objProductoModel.listProducto(0);
      
      FPagoModel objFPagoModel =  new FPagoModel();
      
      ArrayList<FPago> arrayfpago = objFPagoModel.listFpago();
              
      TpoVentaModel objTpoVentaModel = new TpoVentaModel();
      ArrayList<TpoVenta> arraytpoventa = objTpoVentaModel.listTpoVenta();
      
      ReferenciaModel objReferenciaModel = new ReferenciaModel();
      ArrayList<Referencia> arrayreferencia = objReferenciaModel.listTpoReferencia();
      request.getSession().setAttribute("arrayreferencia",arrayreferencia);
      
       
      CliProv objcliprov = new CliProv();
      objcliprov.setCliprovciu("");
      objcliprov.setCliprovcod(0);
      objcliprov.setCliprovcom("");
      objcliprov.setCliprovdir("");
      objcliprov.setCliprovema("");
      objcliprov.setCliprovfon("");
      objcliprov.setCliprovgir("");
      objcliprov.setCliprovraz("");
      objcliprov.setCliprovrut("");
      
      
    request.getSession().setAttribute("servletName",arraylistdocumento);
    request.getSession().setAttribute("arraylistcliprov",arraylistcliprov);
    request.getSession().setAttribute("objcliprov",objcliprov);
    request.getSession().setAttribute("arrayproducto",arrayproducto);   
    
    request.getSession().setAttribute("arraydespacho",arraydespacho);
    request.getSession().setAttribute("arraytraslado",arraytraslado);
         
    
      request.getSession().setAttribute("arrayfpago",arrayfpago);
    
     request.getSession().setAttribute("arraytpoventa",arraytpoventa);
    
    
     request.getSession().setAttribute("modulo", "addStock");
     getServletConfig().getServletContext().getRequestDispatcher("/movimientoview/adminstock.jsp").forward(request,response);
  
       
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
        Logger.getLogger(MovimientoServlet.class.getName()).log(Level.SEVERE, null, ex);
    
    
    }
    
    }
    
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            int empresaid = (int) request.getSession().getAttribute("empresaid");
            String acc = request.getParameter("ACC");
            
            BodegaModel objBodegaModel = new BodegaModel(empresaid);
            ArrayList<Bodega> arraylistbodega = objBodegaModel.listallBodegas();
            request.getSession().setAttribute("arraylistbodega", arraylistbodega);
            ReferenciaModel objReferenciaModel = new ReferenciaModel();
            ArrayList<Referencia> arrayreferencia = objReferenciaModel.listTpoReferencia();
            request.getSession().setAttribute("arrayreferencia",arrayreferencia);
            
            
            
            
            
            request.getSession().setAttribute("ReferenciaFlag",0);
            request.getSession().setAttribute("MovimientoId",0);
            
            DespachoModel objDespacho = new DespachoModel();
            request.getSession().setAttribute("arraydespacho",objDespacho.listDespacho());
            
            DocumentoModel objDocumentoModel;
            objDocumentoModel = new DocumentoModel();
            
            List<Documento> arraylistdocumento = objDocumentoModel.listDocuments();
            CliProvModel objCliProvModel = new CliProvModel(empresaid);
            
            CliProv objCliProv = objCliProvModel.searchCliProv(Integer.parseInt(request.getParameter("CliProvCod")));
            
            
            ArrayList<CliProv> arraylistcliprov = objCliProvModel.listaCliProv(0);
            
            ProductoModel objProductoModel = new ProductoModel(empresaid);
            ArrayList<Producto>  arrayproducto = objProductoModel.listProducto(0);
            request.getSession().setAttribute("arrayproducto",arrayproducto);
            
            
            request.getSession().setAttribute("servletName",arraylistdocumento);
            request.getSession().setAttribute("arraylistcliprov",arraylistcliprov);
            request.getSession().setAttribute("objcliprov",objCliProv);
            request.getSession().setAttribute("referencia","no");
            
            request.getSession().setAttribute("modulo", "addStock");
            
            
            
            
            
            
            
            getServletConfig().getServletContext().getRequestDispatcher("/movimientoview/adminstock.jsp").forward(request,response);
        } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
            Logger.getLogger(adminStock.class.getName()).log(Level.SEVERE, null, ex);
        }
  
        
        }    
    }
    

