package appventas.movimientos;

import java.io.FileNotFoundException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import appventas.include.Conexion;
import appventas.producto.Producto;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class MovimientoModel {
    
    public MovimientoModel() {
      
    }
    
    public String addDocumento(int empresaid,int cliprovid,Movimiento objMovimiento) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            int numdoc = objMovimiento.getNumdoc();
            String fechadoc = objMovimiento.getFechamov();
            int montoafecto = objMovimiento.getMontoafecto();
            int montoexento = objMovimiento.getMontoexento();
            int montoiva = objMovimiento.getMontoiva();
            int montototal = objMovimiento.getMontototal();
            int tipodoc = objMovimiento.getTipodoc();
            String fchref = objMovimiento.getFchref();
            
            
            Despacho objDespacho = objMovimiento.getTipodespacho();
            Traslado objTraslado = objMovimiento.getTipotraslado();
            
            String referencia = objMovimiento.getReferenciaGlobal();
            boolean bolref = objMovimiento.getBolref();
            String strbolref;
            
            if(bolref==false){
                strbolref="no";
                
            }else{
                
                strbolref="si";
            }
            
            
            
            String sql;
            sql="INSERT INTO Movimiento(MovimientoFecha,CliProvId,TipoDocumentoId,NumDoc, \n"+
                    "MovimientoValorNeto,MovimientoExento,MovimientoIva,MovimientoTotalBruto,\n"
                    +"MovimientoTipo,DespachoId,TrasladoId,ReferenciaGlobal,\n"
                    +"TpoDocRef,FolioRef,FchRef,TpoRef,idFpago,IdTpoVenta, EmpresaId, BodegaId, Flag_Stock) \n"+
                    "values('"+fechadoc+"',"+String.valueOf(cliprovid) +","+String.valueOf(tipodoc)+"," +String.valueOf(numdoc)+
                    ","+String.valueOf(montoafecto)+"," + String.valueOf(montoexento)+","+String.valueOf(montoiva) +
                    ","+String.valueOf(montototal)+","+"1"+","+String.valueOf(objDespacho.getDespachoid())+","+String.valueOf(objTraslado.getTipotrasladoid())+",'"+ referencia +"' \n"
                    +","+String.valueOf(objMovimiento.getTpodocref())+","+ String.valueOf(objMovimiento.getFolioref()) +",'"+objMovimiento.getFchref()+"',"+ String.valueOf(objMovimiento.getTporef()) + ","+String.valueOf(objMovimiento.getFpago())+","+String.valueOf(objMovimiento.getTpoventa())+"," + String.valueOf(empresaid) + "," + String.valueOf(objMovimiento.getBodegaid()) +","+ String.valueOf(objMovimiento.getFlag_stock())  + ")";
            
            System.out.print(sql);
            
            Statement stm = objConexion.getConexion().createStatement();
            stm.execute(sql);
            objConexion.cerrar();
            return sql; 
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
     }

    public int searchId(int cliprovid,int iddoc, int numdoc) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            
            
            String sql;
            sql="Select * from Movimiento where CliProvId="+String.valueOf(cliprovid) + "\n"+
                    " and TipoDocumentoId="+String.valueOf(iddoc)+" and NumDoc="+ String.valueOf(numdoc);
            Statement stm = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stm.executeQuery(sql);
            System.out.print(sql);
            objrecordset.next();
            
            int idmovimiento = objrecordset.getInt("MovimientoId");
            
            objConexion.cerrar();
            return idmovimiento;
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    
    
    public int searchDocId(int empresaid, int iddoc, int numdoc) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            String sql;
            sql="Select Movimiento.*, CliProvCod from Movimiento \n"+
                    "inner join CliProv on Movimiento.CliProvId=CliProv.CliProvId"+
                    " where CliProv.EmpresaId="+String.valueOf(empresaid)+
                    " and TipoDocumentoId="+String.valueOf(iddoc)+" and NumDoc="+ String.valueOf(numdoc);
            Statement stm = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stm.executeQuery(sql);
            System.out.print(sql);
            objrecordset.next();
            objConexion.cerrar();
            return objrecordset.getInt("MovimientoId");
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    
    
    
    
    public void addDetalle(int idmovimiento,DetalleMovimiento objdetalle) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            
            
            
            String sql;
            Statement stm = objConexion.getConexion().createStatement();
            BigDecimal cantidad = objdetalle.getCantidad();
            int total = objdetalle.getTotal();
            Producto objproducto = objdetalle.getObjProducto();
            int productoid = objproducto.getProductoid();
            int precio = objproducto.getPrecioventa();
            int descuentopct = objdetalle.getDescuentopct();
            int descuentomonto = objdetalle.getDescuentomonto();
            int preciocosto = objproducto.getPreciocosto();
            sql = "INSERT INTO DetalleMovimiento(MovimientoId,ProductoId,Cantidad,PrecioVenta,PrecioCosto,DescuentoPct,DescuentoMonto, \n"+
                    "TotalDetalle, SaldoAnterior,SaldoActual) values("+String.valueOf(idmovimiento)+","+String.valueOf(productoid)+"\n"+
                    ","+String.valueOf(cantidad)+","+String.valueOf(precio)+","+ String.valueOf(preciocosto) +","  +String.valueOf(descuentopct) +","+String.valueOf(descuentomonto) +","+String.valueOf(total) + "," + String.valueOf(objdetalle.getSaldoanterior())+","+ String.valueOf(objdetalle.getSaldoactual())   +")";        
            stm.execute(sql);
            
            objConexion.cerrar();
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
   
    
    
    
    
    
public ArrayList<Traslado> listTraslado() throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            ArrayList<Traslado> arraylist1 = new  ArrayList<>();
            String sql = "Select * from TipoTraslado";
            Statement stm = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stm.executeQuery(sql);
            while(objrecordset.next()){
                Traslado objTraslado = new Traslado();
                objTraslado.setTipotrasladoid(objrecordset.getInt("TipoTrasladoId"));
                objTraslado.setTrasladocod(objrecordset.getInt("TrasladoCod"));
                objTraslado.setTrasladodes(objrecordset.getString("TrasladoDes"));
                arraylist1.add(objTraslado);
            }
            
            objConexion.cerrar();
            
            
            return arraylist1;
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
}
    

    
public ArrayList<Despacho> listDespacho() throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            ArrayList<Despacho> arraylist1 = new  ArrayList<>();
            String sql = "Select * from TipoDespacho";
            Statement stm = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stm.executeQuery(sql);
            while(objrecordset.next()){
                Despacho objDespacho = new Despacho();
                objDespacho.setDespachoid(objrecordset.getInt("DespachoId"));
                objDespacho.setDespachocod(objrecordset.getInt("DespachoCod"));
                objDespacho.setDespachodes(objrecordset.getString("DespachoDes"));
                arraylist1.add(objDespacho);
            }
            
            objConexion.cerrar();
            
            return arraylist1;
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
}
    


public int getIdDespacho(String tipodespacho) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            
            String sql = "Select * from TipoDespacho  where DespachoDes='"+tipodespacho+"'";
            Statement stm = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stm.executeQuery(sql);
            objrecordset.next();
            objConexion.cerrar();
            return objrecordset.getInt("TipoDespachoId");
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    
}


public int getCodSiiDespacho(int idtipodespacho) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            
            
            String sql = "Select * from TipoDespacho WHERE TipoDespachoId="+String.valueOf(idtipodespacho);
            Statement stm = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stm.executeQuery(sql);
            objrecordset.next();
            objConexion.cerrar();
            return objrecordset.getInt("DespachoCod");
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    
}




public int getIdTraslado(String tipotraslado) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            
            
            String sql = "Select * from TipoTraslado  where TrasladoDes='"+tipotraslado+"'";
            Statement stm = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stm.executeQuery(sql);
            objrecordset.next();
            objConexion.cerrar();
            return objrecordset.getInt("TipoTrasladoId");
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    
}


public int getCodSiiTraslado(int idtipotraslado) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            String sql = "Select * from TipoTraslado WHERE TipoTrasladoId="+String.valueOf(idtipotraslado);
            Statement stm = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stm.executeQuery(sql);
            objrecordset.next();
            objConexion.cerrar();
            return objrecordset.getInt("TrasladoCod");
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
}


public Object[] showDocument(int idmovimiento) throws SQLException{
        try {
            String sql = "select TipoDocumentos.TipoDocumentoDes,TipoDocumentos.CodigoSii, TipoDocumentos.TipoDocumentoId, CliProv.*, Movimiento.MovimientoFecha, \n"+
                    "Movimiento.NumDoc, \n"+
                    "Movimiento.MovimientoValorNeto,Movimiento.MovimientoExento,Movimiento.MovimientoTotalBruto, \n"+
                    "Movimiento.MovimientoIva,Movimiento.MovimientoId, Movimiento.FchRef,Movimiento.FolioRef,\n"+
                    "Movimiento.ReferenciaGlobal, Referencia.ReferenciaCod, T2.CodigoSii as CodSiiDocRef, \n"+
                    "FPago.FPagoSII, Referencia.ReferenciaCod, T2.CodigoSii as CodSiiDocRef, \n"+
                    "Despacho.DespachoCod, Traslado.TrasladoCod \n"+
                    "from Movimiento \n"+
                    "inner Join TipoDocumentos AS T2 on Movimiento.TpoDocRef = T2.TipoDocumentoId \n"+ 
                    "inner Join TipoDocumentos on Movimiento.TipoDocumentoId = TipoDocumentos.TipoDocumentoId \n"+
                    "inner Join Referencia on Movimiento.TpoRef = Referencia.Id \n"+
                    "inner Join CliProv on CliProv.CliProvId = Movimiento.CliProvId \n"+
                    "inner Join FPago on Movimiento.idFPago = FPago.idFPago \n"+
                    "inner Join Despacho on Movimiento.DespachoId = Despacho.DespachoId \n"+
                     "inner Join Traslado on Movimiento.TrasladoId = Traslado.TrasladoId \n"+
                 
                    "where Movimiento.MovimientoId="+String.valueOf(idmovimiento);
            System.out.print(sql);
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            
            
            Statement stm = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stm.executeQuery(sql);
            Object[] auxData = new Object[23];
            
            while(objrecordset.next()){
                auxData[0] = objrecordset.getInt("CliProvCod");
                auxData[1] =  objrecordset.getString("CliProvRut");
                auxData[2] =  objrecordset.getString("CliProvRaz");
                auxData[3] =  objrecordset.getString("CliProvGir");
                auxData[4] =  objrecordset.getString("CliProvDir");
                auxData[5] =  objrecordset.getString("CliProvCom");
                auxData[6] =  objrecordset.getString("CliProvCiu");
                auxData[7] =  objrecordset.getString("TipoDocumentoDes");
                auxData[8] =  objrecordset.getString("CodigoSii");
                auxData[9] =  objrecordset.getInt("NumDoc");
                auxData[10] =  objrecordset.getString("MovimientoFecha");
                auxData[11] =  objrecordset.getInt("MovimientoValorNeto");
                auxData[12] =  objrecordset.getInt("MovimientoExento");
                auxData[13] =  objrecordset.getInt("MovimientoIva");
                auxData[14] =  objrecordset.getInt("MovimientoTotalBruto");
                auxData[15] = objrecordset.getString("FchRef");
                auxData[16] = objrecordset.getString("ReferenciaGlobal");
                auxData[17] = objrecordset.getInt("ReferenciaCod");
                auxData[18] = objrecordset.getInt("CodSiiDocRef");
                auxData[19] = objrecordset.getInt("FolioRef");
                auxData[20] = objrecordset.getInt("FPagoSII");
                auxData[21] = objrecordset.getInt("DespachoCod");
                auxData[22] = objrecordset.getInt("TrasladoCod");
                

            }
            
            objConexion.cerrar();
            return auxData;
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
}







public ArrayList<Object[]> showDetails(int idmovimiento) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            
            
            ArrayList<Object[]> arraylista = new ArrayList<>();
            String sql = "select Producto.ProductoCod, Producto.ProductoNom, DetalleMovimiento.Cantidad,DetalleMovimiento.PrecioVenta,DetalleMovimiento.PrecioCosto, \n"+
                    "DetalleMovimiento.DescuentoPct, \n"+
                    "DetalleMovimiento.TotalDetalle from DetalleMovimiento \n"+
                    "inner join Movimiento on DetalleMovimiento.MovimientoId = Movimiento.MovimientoId \n"+
                    "inner join Producto on DetalleMovimiento.ProductoId = Producto.ProductoId \n"+
                    "where DetalleMovimiento.MovimientoId="+String.valueOf(idmovimiento);
            System.out.print(sql);
            
            Statement stm = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stm.executeQuery(sql);
            
            
            while(objrecordset.next()){
                Object[] auxData = new Object[8];
                auxData[0] = objrecordset.getInt("ProductoCod");
                auxData[1] = objrecordset.getString("ProductoNom");
                auxData[2] = objrecordset.getBigDecimal("Cantidad");
                auxData[3] = objrecordset.getInt("PrecioVenta");
                auxData[4] = objrecordset.getInt("DescuentoPct");
                auxData[6] = objrecordset.getInt("TotalDetalle");
                auxData[7] = objrecordset.getInt("PrecioCosto");
            

                arraylista.add(auxData);
            }
            objConexion.cerrar();
            return arraylista;
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
}



public void addXML(int idmovimiento,String empresarut, int numdoc, int docsiicod) throws SQLException, FileNotFoundException, ParserConfigurationException, SAXException, IOException{ 
  /*  
    objconexion.setAutoCommit(false);
    ConfigAppDTE objconfigclass = new ConfigAppDTE();
    String[] arrayrutemisor = empresarut.split("-");
    String auxrut = arrayrutemisor[0];
    String ruta= objconfigclass.getPathdte()+"DTE"+auxrut+"F"+String.valueOf(numdoc)+"T"+String.valueOf(docsiicod)+".xml";
    String sql = "Update Movimiento set BlobXML=?,Archivo=? where MovimientoId="+String.valueOf(idmovimiento);
    String archivo = "DTE"+auxrut+"F"+String.valueOf(numdoc)+"T"+String.valueOf(docsiicod)+".xml";
    FileInputStream fis = null;
    File file = new File(ruta);
    PreparedStatement ps = null;
    fis = new FileInputStream(file);
    ps = objconexion.prepareStatement(sql);
    ps.setBinaryStream(1,fis,(int)file.length());
    ps.setString(2, archivo);
    ps.executeUpdate();
    objconexion.commit();
    */
}

public void getXML(int idmovimiento) throws IOException, SQLException, ParserConfigurationException, SAXException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            Statement stm = objConexion.getConexion().createStatement();
            ResultSet rs = stm.executeQuery("SELECT BlobXML,Archivo FROM Movimiento where MovimientoId="+String.valueOf(idmovimiento));
            /*
            ConfigAppDTE objconfigclass = new ConfigAppDTE();
            */
            /*
            while (rs.next()){
            Blob objblob = rs.getBlob("BlobXML");
            byte[] data = objblob.getBytes(1, (int)objblob.length());
            try (FileOutputStream fos = new FileOutputStream(objconfigclass.getPathdte()+rs.getString("Archivo"))) {
            fos.write(data);
            fos.close();
            }
            
            }
            */
            objConexion.cerrar();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
}


public void updateTRACKID(int idmovimiento, String trackid) throws SQLException{

        try {
            String sql = "Update Movimiento set MovimientoIdentificadorEnvio="+trackid+" where MovimientoId="+String.valueOf(idmovimiento);
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            System.out.print(sql);
            Statement stm = objConexion.getConexion().createStatement();
            stm.execute(sql);
            objConexion.cerrar();
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
        }

}    

public void updateFlagReferencia(int idmovimiento, int referenciaflag) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            String sql = "Update Movimiento set ReferenciaFlag="+String.valueOf(referenciaflag)+" where MovimientoId="+String.valueOf(idmovimiento);
            Statement stm = objConexion.getConexion().createStatement();
            stm.execute(sql);
            objConexion.cerrar();
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
}    



public void deleteMovimiento(int iddoc) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            Statement stm = objConexion.getConexion().createStatement();
            stm.execute("Delete from DetalleMovimiento where MovimientoId="+ String.valueOf(iddoc));
            stm.execute("Delete from BlobDTE where MovimientoId="+ String.valueOf(iddoc));
            stm.execute("Delete from Movimiento where MovimientoId="+ String.valueOf(iddoc));
            objConexion.cerrar();
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
        }

}




}
