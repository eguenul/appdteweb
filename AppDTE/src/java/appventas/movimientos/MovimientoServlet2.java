
package appventas.movimientos;
import appventas.tpoventa.TpoVenta;
import appventas.tpoventa.TpoVentaModel;
import appventas.bodega.Bodega;
import appventas.bodega.BodegaModel;
import appventas.cliprov.CliProv;
import appventas.cliprov.CliProvModel;
import appventas.documento.Documento;
import appventas.documento.DocumentoModel;
import appventas.fpago.FPago;
import appventas.fpago.FPagoModel;
import appventas.referencia.Referencia;
import appventas.referencia.ReferenciaModel;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class MovimientoServlet2 extends HttpServlet {
    
@Override
public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
    if(request.getSession().getAttribute("loginauth")==null){
        request.getRequestDispatcher("login").forward(request, response);                                        
    }
    request.getSession().setAttribute("coddocsii", null);  
    
        
    }

 @Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
      
    try { 
        
        String acc = request.getParameter("ACC");
   
        
        
         
        int empresaid = (int) request.getSession().getAttribute("empresaid"); 
        CliProvModel objCliProvModel = new CliProvModel(empresaid);
        CliProv objCliProv = objCliProvModel.searchCliProv(Integer.parseInt(request.getParameter("CliProvCod")));
        request.getSession().setAttribute("CliProvCod", objCliProv.getCliprovcod());
        
      
         
         
         
        
         
         if("SELECT".equals(acc)){
       request.getSession().setAttribute("coddocsii", null);  
       
         BodegaModel objBodegaModel = new BodegaModel(empresaid);
           ArrayList<Bodega> arraylistbodega = objBodegaModel.listallBodegas();
           request.getSession().setAttribute("arraylistbodega", arraylistbodega);
       
      
             
             
             
              request.getSession().setAttribute("referencia","yes");
             int idmovimiento = Integer.parseInt(request.getParameter("Id"));
             MovimientoModel2 objMovimientoModel = new MovimientoModel2();
             Movimiento objMovimiento = objMovimientoModel.getData(idmovimiento);
             
             
            
              request.getSession().setAttribute("ReferenciaFlag",1);
              request.getSession().setAttribute("MovimientoId",idmovimiento);
               
             
             
             
             
            request.getSession().setAttribute("objMovimiento", objMovimiento);
             objCliProv = objCliProvModel.searchCliProv(Integer.parseInt(request.getParameter("CliProvCod")));
             request.getSession().setAttribute("objcliprov", objCliProv);           
        
             ArrayList<DetalleMovimiento> arraydetalle = objMovimientoModel.listDetalle(idmovimiento);
             request.getSession().setAttribute("arraydetalle", arraydetalle);
             Object[] arrayreferencia = objMovimientoModel.getReferencia(idmovimiento);
            
             int numdoc = (int) arrayreferencia[1];
             String fechadoc = (String) arrayreferencia[0];
             String docdes = (String) arrayreferencia[2];
             int codsii = (int) arrayreferencia[3];
             int tipodocref = (int) arrayreferencia[4];
     
             request.getSession().setAttribute("numdoc", numdoc);
             request.getSession().setAttribute("docdes", docdes);
             request.getSession().setAttribute("codsii", codsii);
             request.getSession().setAttribute("fechadoc", fechadoc);
             request.getSession().setAttribute("tipodocref", tipodocref);
             
             
             
              DespachoModel objDespacho = new DespachoModel();    
              List<Despacho> arraydespacho = objDespacho.listDespacho();
          
       
              TrasladoModel objTraslado = new TrasladoModel();    
              List<Traslado> arraytraslado = objTraslado.listTraslado();
  
          
         
              request.getSession().setAttribute("arraydespacho",arraydespacho);
              request.getSession().setAttribute("arraytraslado",arraytraslado);
             
          
               DocumentoModel objDocumentoModel2;
               objDocumentoModel2 = new DocumentoModel();
               List<Documento> arraylistdocumento2 = objDocumentoModel2.listDocuments();
        
        
        request.getSession().setAttribute("servletName",arraylistdocumento2);
             
        
     FPagoModel objFPagoModel =  new FPagoModel();
      
      ArrayList<FPago> arrayfpago = objFPagoModel.listFpago();
              
      TpoVentaModel objTpoVentaModel = new TpoVentaModel();
      ArrayList<TpoVenta> arraytpoventa = objTpoVentaModel.listTpoVenta();
      
        request.getSession().setAttribute("arrayfpago",arrayfpago);
    
     request.getSession().setAttribute("arraytpoventa",arraytpoventa);
    
       
     
     
     
        String CodSiiDoc = request.getParameter("CodSii");
        DocumentoModel objDocumento = new DocumentoModel();
     
     
     
     
         int tipodocumento = objDocumento.getIddocumento2(Integer.parseInt(CodSiiDoc));
            
            
         String nombredoc =  objDocumento.getNombreDoc(tipodocumento);
         request.getSession().setAttribute("nombredoc",nombredoc);  
         request.getSession().setAttribute("TipoDocumento",tipodocumento);
        
      ReferenciaModel objReferenciaModel = new ReferenciaModel();
      ArrayList<Referencia> arrayreferencia2 = objReferenciaModel.listTpoReferencia();
      request.getSession().setAttribute("arrayreferencia",arrayreferencia2);
      
            
             //   PrintWriter out = response.getWriter(); 
             //ya podemos enviar al navegador
             // out.println(request.getSession().getAttribute("numdoc", numdoc));
             
             

  getServletConfig().getServletContext().getRequestDispatcher("/movimientoview/addmovimiento.jsp").forward(request,response);
   
             //  PrintWriter out = response.getWriter(); 
             //ya podemos enviar al navegador
           //  out.println(idmovimiento);
         }
         
         

        
        
} catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
     
         PrintWriter out = response.getWriter(); 
                   //ya podemos enviar al navegador
                                   out.println(ex.getMessage());
       
    
    
    Logger.getLogger(MovimientoServlet2.class.getName()).log(Level.SEVERE, null, ex);
    }
  
                

}
   

}
