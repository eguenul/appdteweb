/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.movimientos;

import appventas.include.Conexion;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class StockModel {
  
    
    
public void addStock(int idBodega, int idProducto, BigDecimal cantidad){
    try {
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        Statement stm = objConexion.getConexion().createStatement();
        if(consultaExistencia(idBodega,idProducto)==false){        
            stm.execute("Insert into Stock (BodegaId,ProductoId, SaldoDisponible) values("+String.valueOf(idBodega)+","+String.valueOf(idProducto)+","+ String.valueOf(cantidad)  +")");
        }else{
        
        stm.execute("Update Stock set SaldoDisponible=SaldoDisponible+"+String.valueOf(cantidad)+ " Where BodegaId="+String.valueOf(idBodega) + " and ProductoId=" + String.valueOf(idProducto));
        }
        objConexion.cerrar();
        
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(StockModel.class.getName()).log(Level.SEVERE, null, ex);
    }
      
      
      
}

public void lowStock(int idBodega, int idProducto, BigDecimal cantidad){
    
    try {
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        Statement stm = objConexion.getConexion().createStatement();
        
        stm.execute("Update Stock set SaldoDisponible=SaldoDisponible-"+String.valueOf(cantidad)+ " Where BodegaId="+String.valueOf(idBodega) + " and ProductoId=" + String.valueOf(idProducto));
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(StockModel.class.getName()).log(Level.SEVERE, null, ex);
    }
        
      
      
      
}
  
  
   
  public BigDecimal getSaldo(int idBodega, int idProducto){
     BigDecimal SaldoDisponible = new BigDecimal("0");
      
      try {
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        
        if(consultaExistencia(idBodega,idProducto)==true){
            
            String sql = "Select * from Stock where BodegaId="+String.valueOf(idBodega) + " AND ProductoId=" + String.valueOf(idProducto) ;
            
            
            System.out.print(sql);
            Statement stm = objConexion.getConexion().createStatement();
            ResultSet objrecordset = stm.executeQuery(sql);
            
            while(objrecordset.next()){
                SaldoDisponible = objrecordset.getBigDecimal("SaldoDisponible");
             System.out.print("SALDO DISPONIBLE ES "+ String.valueOf(objrecordset.getInt("SaldoDisponible")));   
            }
            
        }else{
             SaldoDisponible = new BigDecimal("0");
            
        }
        
        
        
        
        objConexion.cerrar();
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        SaldoDisponible = new BigDecimal("0");
        Logger.getLogger(StockModel.class.getName()).log(Level.SEVERE, null, ex);
        
    }
       return SaldoDisponible;
       
} 
  
  
public boolean consultaExistencia(int idBodega, int idProducto) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
      Conexion objConexion = new Conexion();
      objConexion.Conectar();
      boolean flag_stock = false;
      String sql = "Select * from Stock where BodegaId="+String.valueOf(idBodega) + " AND ProductoId=" + String.valueOf(idProducto) ;
        
        
      System.out.print(sql);
      Statement stm = objConexion.getConexion().createStatement();
      ResultSet objrecordset = stm.executeQuery(sql);
      
      
      flag_stock = objrecordset.next();
      objConexion.cerrar();
      return flag_stock;
} 
  
public void disminuyeStock(){
          
          
}
  
   
   
   
  
}
