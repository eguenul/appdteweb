package appventas.movimientos;

import appventas.cliprov.CliProv;
import appventas.cliprov.CliProvModel;
import appventas.documento.DocumentoModel;
import appventas.empresa.Empresa;
import appventas.empresa.EmpresaModel;
import appventas.include.Funciones;
import appventas.include.PrintNota;
import appventas.producto.Producto;
import appventas.producto.ProductoModel;
import appventas.usuarios.Usuario;
import appventas.usuarios.UsuarioModel;
import com.appdte.sii.utilidades.ConfigAppDTE;
import com.appdte.sii.utilidades.FuncionesCAF;
import com.appdte.sii.utilidades.PrintBOLETA;
import com.appdte.sii.utilidades.PrintDTE;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class addMovimientoServlet extends HttpServlet { 
    @Override
     public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         try {
                int empresaid = (int) request.getSession().getAttribute("empresaid");
    
               
       if(request.getSession(true).getAttribute("loginauth")==null){
         request.getRequestDispatcher("login").forward(request, response); 
       }
       
       /* CONSULTA CERTIFICADO DIGITAL */
           ConfigAppDTE objConfigAppDTE = new ConfigAppDTE();
           String login = (String) request.getSession().getAttribute("login");
           String sFichero = objConfigAppDTE.getPathcert()+login+".pfx";
           File fichero = new File(sFichero);
             
           if(!fichero.exists()){
                  response.sendRedirect("messageview/errorcertificado.html");
                
              }
           
           
           
           
           int idBodega = Integer.parseInt(request.getParameter("Bodega"));
           int flag_stock = Integer.parseInt(request.getParameter("flag_stock"));
            
             
        
           
           for(int y=0; y<Integer.parseInt(request.getParameter("NRO_FILAS"));y++){
               System.out.print("validando stock");
                 System.out.print(String.valueOf(flag_stock));
             
               int productocod = Integer.parseInt(request.getParameter("ProductoCod"+String.valueOf(y)));
               BigDecimal cantidad  = new BigDecimal(request.getParameter("Cantidad"+String.valueOf(y)));
               
               ProductoModel objProductoModel = new ProductoModel(empresaid);
               Producto objProducto = new Producto();      
               objProducto =  objProductoModel.searchProducto(productocod);
               System.out.print(objProducto.getProductostock());
               if(flag_stock==1 && objProducto.getProductostock()==1 ){
                   StockModel objStockModel =  new StockModel();
                       if(cantidad.compareTo(objStockModel.getSaldo(idBodega, objProducto.getProductoid()))>=0){
                           System.out.print("ERROR STOCK");
                           response.sendRedirect("messageview/errorstock.html");
                           
                       }    
               }
               
            }
              
            boolean flag_dte = true;
          
           
           
           
       
            CliProvModel objCliProvModel = new CliProvModel(empresaid);      
            CliProv objCliProv = objCliProvModel.searchCliProv(Integer.parseInt(request.getParameter("CliProvCod")));
            Movimiento objMovimiento = new Movimiento();
            objMovimiento.setObjcliprov(objCliProv);
            int exento = Integer.parseInt(request.getParameter("Exento"));
            int iva = Integer.parseInt(request.getParameter("Iva"));
            int totalneto = Integer.parseInt(request.getParameter("TotalNeto"));
            int totalbruto = Integer.parseInt(request.getParameter("TotalBruto"));
            int tipodocumento =  Integer.parseInt(request.getParameter("TipoDocumento"));
            int fpago = Integer.parseInt(request.getParameter("FPago"));
            int tpoventa =  Integer.parseInt(request.getParameter("TpoVenta"));
          
            
            
            objMovimiento.setFpago(fpago);
            objMovimiento.setTpoventa(tpoventa);
            
            objMovimiento.setFlag_stock(flag_stock);
            objMovimiento.setTipodoc(tipodocumento);
            DocumentoModel objDocumento = new DocumentoModel();
            String fechadoc = request.getParameter("FechaDoc");
            String codsii = String.valueOf(objDocumento.getSiiCod(tipodocumento));
            String campo="";
           
         
            String fchref =(String)request.getParameter("FchRef");
            
            objMovimiento.setFchref(fchref);
             
            
            int  tipodespachoid = Integer.parseInt(request.getParameter("TipoDespacho"));
            int   tipotrasladoid = Integer.parseInt(request.getParameter("TipoTraslado"));
            
            
             /* Despacho objDespacho = new Despacho(); */
           DespachoModel objDespachoModel = new DespachoModel();
           Despacho objDespacho = objDespachoModel.getData(tipodespachoid);
           objMovimiento.setTipodespacho(objDespacho);
           
           TrasladoModel objTrasladoModel = new TrasladoModel();
           Traslado objTraslado = objTrasladoModel.getData(tipotrasladoid);
           
           objMovimiento.setTipotraslado(objTraslado);
           objMovimiento.setReferenciaGlobal(request.getParameter("Observacion"));
           objMovimiento.setTpodocref(Integer.parseInt(request.getParameter("TpoDocRef")));
           objMovimiento.setFolioref(Integer.parseInt(request.getParameter("NumDocRef")));
           objMovimiento.setTporef(Integer.parseInt(request.getParameter("TpoRef")));
           objMovimiento.setBodegaid(idBodega);
           /*
            objDespacho.setDespachoid(tipodespachoid);
            Traslado objTraslado = new Traslado();
             */       
            switch (codsii) {
            
               case "33":
                         campo = "FacVentaAfecta";
                         break;
                       
               case "52":
                         campo = "GuiaDespacho";   
                         break;
               case "56":
                         campo = "NotaDebito";   
                         break;
               
               
               case "61":
                         campo = "NotaCredito";
                         break;
            
                 case "39":
                         campo = "BolAfectaE";
                         break;
                 
                 case "41":
                         campo = "BolExentaE";
                         break;     
                         
              
               
               case "34":
                         campo = "FacVentaExenta";
                         break;
                         
                         
               case "802":
                         campo = "NotaPedido";
                         flag_dte = false;
                         break;          
               
               case "46":
                         
                         response.sendRedirect("messageview/errordoc.html");
                         break;          
                
               case "801":
                         
                         response.sendRedirect("messageview/errordoc.html");
                         break;                    
           }
           
          
          
          boolean validacionform = true;
          
          
            Funciones objfunciones = new Funciones();
          
            if(flag_dte ==true){
            Funciones objFunciones = new Funciones();
            objFunciones.loadCAF(empresaid, tipodocumento, login);
                
                
            if(objfunciones.buscaFolios(login,empresaid, codsii)==false){
                validacionform = false;
                  response.sendRedirect("messageview/errorfolio.html");
            }
            }
        
            
             if(("34".equals(codsii)) & (totalneto > 0)){
                  validacionform = false;
                 response.sendRedirect("messageview/errormontoafecto.html");
            }
         
              if(("33".equals(codsii)) & (totalneto==0)){
                  validacionform = false;
                 response.sendRedirect("messageview/errormontoafecto2.html");
            }
         
             
            ConfigAppDTE objConfig = new ConfigAppDTE();
          
            int numcorrelativo =  objDocumento.searchCorrelativo(empresaid, campo);
            objMovimiento.setFechamov(fechadoc);
            objMovimiento.setNumdoc(numcorrelativo);
            objMovimiento.setMontoexento(exento);
            objMovimiento.setMontoiva(iva);
            objMovimiento.setMontototal(totalbruto);
            objMovimiento.setMontoafecto(totalneto);
            MovimientoModel objMovimientoModel = new MovimientoModel();
            
           EmpresaModel objEmpresaModel = new EmpresaModel();
          
            Empresa objEmpresa = objEmpresaModel.getData(empresaid);
          
           FuncionesCAF objCAF = new FuncionesCAF();
          
           
           if(flag_dte==true){
            if (objCAF.validaCAF(login,objConfig.getPathcaf(), objEmpresa.getEmpresarut(),Integer.parseInt(codsii),numcorrelativo)==false){
                validacionform = false;
                response.sendRedirect("messageview/errorcaf.html");
             
            }
           }
            
           if(validacionform==true){ 
            
            objMovimientoModel.addDocumento(empresaid, objCliProv.getCliprovid(), objMovimiento);
            
          int referenciaflag = (int) request.getSession().getAttribute("ReferenciaFlag");
          int auxidmovimiento = (int) request.getSession().getAttribute("MovimientoId");
            
          if(referenciaflag==1){
              objMovimientoModel.updateFlagReferencia(auxidmovimiento, referenciaflag);
          }
            
            
            
            
            
            
            
            
            
            int idmovimiento = objMovimientoModel.searchId(objCliProv.getCliprovid(), tipodocumento, numcorrelativo);
            objDocumento.updateCorrelativo(empresaid, campo);
           
           // ArrayList<DetalleMovimiento> arraydetalle = new ArrayList<>();           
            int nrofilas = Integer.parseInt(request.getParameter("NRO_FILAS"));
             
              
            for(int i=0;i<nrofilas;i++){
               int productocod = Integer.parseInt(request.getParameter("ProductoCod"+String.valueOf(i)));
               ProductoModel objProductoModel = new ProductoModel(empresaid);
               
               
                StockModel objStock = new StockModel();
               int bodegaid = Integer.parseInt(request.getParameter("Bodega")); 
               Producto objProducto =  objProductoModel.searchProducto(productocod);
               int idproducto = objProducto.getProductoid();
             
               
               
               
               
              
               DetalleMovimiento objDetalleMovimiento = new DetalleMovimiento();
               objDetalleMovimiento.setObjProducto(objProducto);
               objDetalleMovimiento.setCantidad(new BigDecimal(request.getParameter("Cantidad"+String.valueOf(i))));
               objDetalleMovimiento.setTotal(Integer.parseInt(request.getParameter("Total"+String.valueOf(i))));
             
               /* ejecuto la actualizacion del stock en este caso reduccion */
              if(!"61".equals(codsii) && flag_stock==1 ){
               BigDecimal SaldoAnterior = objStock.getSaldo(bodegaid, idproducto);
               objDetalleMovimiento.setSaldoanterior(SaldoAnterior);
               objStock.lowStock(bodegaid, idproducto,objDetalleMovimiento.getCantidad());
               BigDecimal SaldoActual = objStock.getSaldo(bodegaid, idproducto);
               objDetalleMovimiento.setSaldoactual(SaldoActual);
              }
                  
             
              if("61".equals(codsii) && flag_stock==1){
               BigDecimal SaldoAnterior = objStock.getSaldo(bodegaid, idproducto);
               objDetalleMovimiento.setSaldoanterior(SaldoAnterior);
               objStock.addStock(bodegaid, idproducto,objDetalleMovimiento.getCantidad());
               BigDecimal SaldoActual = objStock.getSaldo(bodegaid, idproducto);
               objDetalleMovimiento.setSaldoactual(SaldoActual);
          
              }
              
              if( flag_stock==0){
               BigDecimal SaldoAnterior = objStock.getSaldo(bodegaid, idproducto);
               objDetalleMovimiento.setSaldoanterior(SaldoAnterior);
               BigDecimal SaldoActual = objStock.getSaldo(bodegaid, idproducto);
               objDetalleMovimiento.setSaldoactual(SaldoActual);
          
              }
              
              
              
        
               objMovimientoModel.addDetalle(idmovimiento, objDetalleMovimiento);
            }
            /* inicio la secuencia de crear el xml */
          
            if(flag_dte==true){
            
            
            MovimientoController objMovimientoController;
           objMovimientoController = new MovimientoController();
           
           
          /* 
           String login = (String) request.getSession().getAttribute("login");
           */
           UsuarioModel objUsuarioModel = new UsuarioModel();
           
           Usuario objUsuario = objUsuarioModel.getUsuario(login);
           
           
           String trackid = objMovimientoController.sendDTE(objUsuario, empresaid, idmovimiento);
           
        if("0".equals(trackid)==true ){
               objMovimientoModel.deleteMovimiento(idmovimiento);
              response.sendRedirect("messageview/errorenvio.html");
            
        }else{
           objMovimientoModel.updateTRACKID(idmovimiento, trackid);  
          
           
           String rutempresa = objEmpresa.getEmpresarut();
           
           
         
           
           
         /* preparo la impresion del documento */
            BlobDTE objblob = new BlobDTE();
            objblob.getXMLDTE(idmovimiento);
         
          String[] arrayrutemisor = rutempresa.split("-");
         
          if("39".equals(codsii) || "41".equals(codsii)){
               PrintBOLETA objPrint = new PrintBOLETA();
            objPrint.printBOLETA(arrayrutemisor[0]+"F"+String.valueOf(numcorrelativo)+"T"+codsii);
              
          }else{
          PrintDTE objPrint = new PrintDTE();
           objPrint.printDTE(arrayrutemisor[0]+"F"+String.valueOf(numcorrelativo)+"T"+codsii);
        
          }
       
          //request.getSession().setAttribute("nombre_param", "valor_param");
           request.getSession().setAttribute("trackid",trackid);
           request.getSession().setAttribute("nombredocumento","ENVDTE"+arrayrutemisor[0]+"F"+String.valueOf(numcorrelativo)+"T"+codsii);
           request.getSession().setAttribute("tipovista","emision");
           response.sendRedirect("movimientoview/successfull.jsp");
          } 
        }else{  
                    //se indica que la respuesta sera HTML
                    
                    
         PrintNota objPrint = new PrintNota();
         objPrint.printPDF(idmovimiento, empresaid);
                    
         request.getSession().setAttribute("nombredocumento","NOTAPEDIDO");      
         response.sendRedirect("movimientoview/shownota.jsp");
            
            
         }    
        }
         } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
             Logger.getLogger(addMovimientoServlet.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IOException | NumberFormatException ex) {
            Logger.getLogger(addMovimientoServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
           
            Logger.getLogger(addMovimientoServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
                
            
     
     }
     

}
