/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.movimientos;

import appventas.tpoventa.TpoVentaModel;
import appventas.cliprov.CliProv;
import appventas.cliprov.CliProvModel;
import appventas.documento.DocumentoModel;
import appventas.fpago.FPagoModel;
import appventas.include.ConfigAppVenta;
import appventas.include.PrintStock;
import appventas.producto.Producto;
import appventas.producto.ProductoModel;
import appventas.report.Report;
import com.appdte.sii.utilidades.ConfigAppDTE;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import net.sf.jasperreports.engine.JRException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class addStock extends HttpServlet{
       
   
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            
            DocumentoModel objDocumentoModel = new DocumentoModel();
            
          
            int empresaid = (int) request.getSession().getAttribute("empresaid");
            CliProvModel objCliProvModel = new CliProvModel(empresaid);
            CliProv objCliProv = objCliProvModel.searchCliProv(Integer.parseInt(request.getParameter("CliProvCod")));
            Movimiento objMovimiento = new Movimiento();
            objMovimiento.setObjcliprov(objCliProv);
            int exento = Integer.parseInt(request.getParameter("Exento"));
            int iva = Integer.parseInt(request.getParameter("Iva"));
            int totalneto = Integer.parseInt(request.getParameter("TotalNeto"));
            int totalbruto = Integer.parseInt(request.getParameter("TotalBruto"));
            int tipodocumento =  objDocumentoModel.getIdNotacion("addSTOCK");
            
            
            FPagoModel objFPagoModel = new FPagoModel();
            TpoVentaModel objTpoVentaModel = new TpoVentaModel();
            
            
            int fpago = objFPagoModel.getFPagoId(1);
            int tpoventa = objTpoVentaModel.getTpoVentaId(1);
            
            
            
            objMovimiento.setFpago(fpago);
            objMovimiento.setTpoventa(tpoventa);
            
            objMovimiento.setFlag_stock(1);
            objMovimiento.setTipodoc(tipodocumento);
            DocumentoModel objDocumento = new DocumentoModel();
            String fechadoc = request.getParameter("FechaDoc");
            String campo="addSTOCK";
            
            
            MovimientoModel objMovimientoModel = new MovimientoModel();
            
            int numcorrelativo =  objDocumento.searchCorrelativo(empresaid, campo);
            
            objDocumento.updateCorrelativo(empresaid, campo);
           
            objMovimiento.setFechamov(fechadoc);
            objMovimiento.setNumdoc(numcorrelativo);
            objMovimiento.setMontoexento(exento);
            objMovimiento.setMontoiva(iva);
            objMovimiento.setMontototal(totalbruto);
            objMovimiento.setMontoafecto(totalneto);
            objMovimiento.setBodegaid(Integer.parseInt(request.getParameter("BodegaId")));
         
            
            
            
            
            String fchref =(String)request.getParameter("FchRef");
            
            objMovimiento.setFchref(fchref);
             
            DespachoModel objDespachoModel = new DespachoModel();
            TrasladoModel objTrasladoModel = new TrasladoModel();
            
            
            int  tipodespachoid = objDespachoModel.getDespachoId(1);
            int   tipotrasladoid = objTrasladoModel.getId(1);
            
            
            /* Despacho objDespacho = new Despacho(); */
            
            Despacho objDespacho = objDespachoModel.getData(tipodespachoid);
            objMovimiento.setTipodespacho(objDespacho);
            
            
            
            
            
            Traslado objTraslado = objTrasladoModel.getData(tipotrasladoid);
            
            
            
            
            objMovimiento.setTipotraslado(objTraslado);
            objMovimiento.setReferenciaGlobal(request.getParameter("Observacion"));
            objMovimiento.setTpodocref(Integer.parseInt(request.getParameter("TpoDocRef")));
            objMovimiento.setFolioref(Integer.parseInt(request.getParameter("NumDocRef")));
            objMovimiento.setFlag_stock(1);
          /*
            String fchref =(String)request.getParameter("FchRef");
            */
            
            objMovimiento.setTporef(Integer.parseInt(request.getParameter("TpoDocRef")));
            
            objMovimientoModel.addDocumento(empresaid, objCliProv.getCliprovid(), objMovimiento);
           
            int nrofilas = Integer.parseInt(request.getParameter("NRO_FILAS"));
            
            
                   int idmovimiento = objMovimientoModel.searchId(objCliProv.getCliprovid(), tipodocumento, numcorrelativo);
    
              
           for(int i=0;i<nrofilas;i++){
               int productocod = Integer.parseInt(request.getParameter("ProductoCod"+String.valueOf(i)));
               ProductoModel objProductoModel = new ProductoModel(empresaid);
               Producto objProducto = new Producto();      
               StockModel objStock = new StockModel();
               int bodegaid = Integer.parseInt(request.getParameter("BodegaId")); 
               objProducto =  objProductoModel.searchProducto(productocod);
             
               int idproducto = objProducto.getProductoid();
               DetalleMovimiento objDetalleMovimiento = new DetalleMovimiento();
               objDetalleMovimiento.setObjProducto(objProducto);
               objDetalleMovimiento.setCantidad(new BigDecimal( request.getParameter("Cantidad"+String.valueOf(i))));
               objDetalleMovimiento.setTotal(Integer.parseInt(request.getParameter("Total"+String.valueOf(i))));
               BigDecimal SaldoAnterior = objStock.getSaldo(bodegaid, idproducto);
               objDetalleMovimiento.setSaldoanterior(SaldoAnterior);
               objStock.addStock(bodegaid, idproducto,objDetalleMovimiento.getCantidad());
               BigDecimal SaldoActual = objStock.getSaldo(bodegaid, idproducto);
               objDetalleMovimiento.setSaldoactual(SaldoActual);
               objMovimientoModel.addDetalle(idmovimiento, objDetalleMovimiento);
            }
           /*
             PrintStock objPrint = new PrintStock();
             objPrint.printPDF(idmovimiento, empresaid, "NOTA CARGA DE STOCK");
             */
            ConfigAppVenta objconfig = new ConfigAppVenta();
            ConfigAppDTE objconfigdte = new ConfigAppDTE();
            Report objReport = new Report("IngresoStock",objconfig.getPathdownload(),"IngresoStock"+String.valueOf(empresaid) +"."+"pdf", objconfigdte.getPathpdf());
           objReport.setParameters("MovimientoId", String.valueOf(idmovimiento));
            objReport.showReport();
            request.getSession().setAttribute("nombredocumento", "IngresoStock"+String.valueOf(empresaid)); 
            response.sendRedirect("movimientoview/showstock.jsp");
            
        } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException | NumberFormatException | JRException ex) {
            Logger.getLogger(addStock.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
