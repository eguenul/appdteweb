/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appventas.movimientos;

import appventas.producto.Producto;
import java.math.BigDecimal;


/**
 *
 * @author esteban
 */
public class DetalleMovimiento {
private BigDecimal cantidad; 
private int descuentopct;
private int descuentomonto;
private int total;
private BigDecimal saldoanterior;
private BigDecimal saldoactual;
Producto objProducto;


public DetalleMovimiento(){
    
  this.saldoanterior= new BigDecimal(0);
  this.saldoactual= new BigDecimal(0);
  
}

    public Producto getObjProducto() {
        return objProducto;
    }

    public void setObjProducto(Producto objProducto) {
        this.objProducto = objProducto;
    }
    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public int getDescuentopct() {
        return descuentopct;
    }

    public void setDescuentopct(int descuentopct) {
        this.descuentopct = descuentopct;
    }

    public int getDescuentomonto() {
        return descuentomonto;
    }

    public void setDescuentomonto(int descuentomonto) {
        this.descuentomonto = descuentomonto;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public BigDecimal getSaldoanterior() {
        return saldoanterior;
    }

    public BigDecimal getSaldoactual() {
        return saldoactual;
    }

    public void setSaldoanterior(BigDecimal saldoanterior) {
        this.saldoanterior = saldoanterior;
    }

    public void setSaldoactual(BigDecimal saldoactual) {
        this.saldoactual = saldoactual;
    }





}
