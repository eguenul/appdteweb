/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appventas.movimientos;

import appventas.include.Conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class TrasladoModel {
 
    
public ArrayList<Traslado> listTraslado() throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
   Conexion objConexion = new Conexion();
        objConexion.Conectar();
       
    
    
    
    ArrayList<Traslado> arraylist1 = new  ArrayList<>();
    String sql = "Select * from Traslado";     
    Statement stm = objConexion.getConexion().createStatement();
  
    ResultSet objrecordset = stm.executeQuery(sql);
    while(objrecordset.next()){
        Traslado objTraslado = new Traslado();
        objTraslado.setTipotrasladoid(objrecordset.getInt("TrasladoId"));
        objTraslado.setTrasladocod(objrecordset.getInt("TrasladoCod"));
        objTraslado.setTrasladodes(objrecordset.getString("TrasladoDes"));
        arraylist1.add(objTraslado);
    }
    objConexion.cerrar();
    
    
    return arraylist1;
}
    
    
    
public Traslado getData(int tipotrasladoid) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
    
    Conexion objConexion = new Conexion();
        objConexion.Conectar();
       
    
      String sql = "Select * from Traslado where TrasladoId="+String.valueOf(tipotrasladoid);
      Statement stm = objConexion.getConexion().createStatement();
      ResultSet objrecordset = stm.executeQuery(sql);
      Traslado objTraslado = new Traslado(); 
    
    while(objrecordset.next()){
      objTraslado.setTipotrasladoid(objrecordset.getInt("TrasladoId"));
      objTraslado.setTrasladocod(objrecordset.getInt("TrasladoCod"));
      objTraslado.setTrasladodes(objrecordset.getString("TrasladoDes"));
    }
    objConexion.cerrar();
      return objTraslado;
      
}

public int getId(int parmcodsii) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
    
    Conexion objConexion = new Conexion();
        objConexion.Conectar();
       
      int trasladoid = 0;
      String sql = "Select * from Traslado where TrasladoCod="+parmcodsii;
      Statement stm = objConexion.getConexion().createStatement();
      ResultSet objrecordset = stm.executeQuery(sql);
     
    while(objrecordset.next()){
      trasladoid =  objrecordset.getInt("TrasladoId");
    }
    objConexion.cerrar();
      return trasladoid;
      
}








    
}
