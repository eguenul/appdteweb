/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appventas.movimientos;

import appventas.include.Conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class DespachoModel {
  
  
  public ArrayList<Despacho> listDespacho() throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
      Conexion objConexion = new Conexion();
       objConexion.Conectar();
    
      ArrayList<Despacho> arraydespacho = new ArrayList<>();
      String sql = "Select * from Despacho";
      Statement stm = objConexion.getConexion().createStatement();
      ResultSet objrecordset = stm.executeQuery(sql);
     
      while( objrecordset.next()){
          Despacho objdespacho = new Despacho();
          objdespacho.setDespachocod(objrecordset.getInt("DespachoCod"));
          objdespacho.setDespachodes(objrecordset.getString("DespachoDes"));
          objdespacho.setDespachoid(objrecordset.getInt("DespachoId"));
          arraydespacho.add(objdespacho);
      }
      objConexion.cerrar();
      return arraydespacho;
  
  }
  
  public Despacho getData(int tipodespachoid) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
     Conexion objConexion = new Conexion();
            objConexion.Conectar();
    
      String sql = "Select * from Despacho where DespachoId="+String.valueOf(tipodespachoid);
      System.out.print(sql);
      Statement stm = objConexion.getConexion().createStatement();
      ResultSet objrecordset = stm.executeQuery(sql);
      Despacho objDespacho = new Despacho(); 
    while(objrecordset.next()){
      objDespacho.setDespachoid(objrecordset.getInt("DespachoId"));
      objDespacho.setDespachocod(objrecordset.getInt("DespachoCod"));
      objDespacho.setDespachodes(objrecordset.getString("DespachoDes"));
    }
     objConexion.cerrar();
      return objDespacho;
  }
  
  
  public int getDespachoId(int parmcodsii) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
     Conexion objConexion = new Conexion();
     objConexion.Conectar();
    int despachoid = 0;
      String sql = "Select * from Despacho where DespachoCod="+String.valueOf(parmcodsii);
      System.out.print(sql);
      Statement stm = objConexion.getConexion().createStatement();
      ResultSet objrecordset = stm.executeQuery(sql);
     
    while(objrecordset.next()){
      despachoid = objrecordset.getInt("DespachoId");
    }
     objConexion.cerrar();
      return despachoid;
  }
  
  
  
  
  
  
  
  
  
  
}
