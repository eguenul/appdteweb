/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.report;

import appventas.bodega.Bodega;
import appventas.bodega.BodegaModel;
import appventas.include.ConfigAppVenta;
import appventas.movimientos.MovimientoModel2;
import appventas.producto.Producto;
import appventas.producto.ProductoModel;
import com.appdte.sii.utilidades.ConfigAppDTE;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import net.sf.jasperreports.engine.JRException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class MovimientoProd extends HttpServlet {
@Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
        int empresaid = (int) request.getSession().getAttribute("empresaid");
        request.getSession().setAttribute("empresaid", empresaid);
        
        MovimientoModel2  objMovimientoModel = new MovimientoModel2();
        String fechadesde = request.getParameter("FechaDesde");
        String fechahasta = request.getParameter("FechaHasta");
        String productocod = request.getParameter("ProductoCod");
        String BodegaId = request.getParameter("BodegaId");
        String extension;
        extension =request.getParameter("TipoReporte");
        /*
        int nroregistros = objMovimientoModel.conteoMovimiento(fechadesde, fechahasta, empresaid);
        */
        /*
        if(nroregistros>0){
            */
            System.out.print(empresaid);
            String fecha1 = fechadesde;
            
            
            
            String fecha2 = fechahasta;
            
            ConfigAppVenta objconfig = new ConfigAppVenta();
            ConfigAppDTE objconfigdte = new ConfigAppDTE();
            Report objReport = new Report("MovimientoProducto",objconfig.getPathdownload(),"MovimientoProducto"+String.valueOf(empresaid) +"."+extension, objconfigdte.getPathpdf());
          
            
            request.getSession().setAttribute("nombredocumento", "MovimientoProducto"+String.valueOf(empresaid));
            objReport.setParameters("FechaDesde", fecha1);
            objReport.setParameters("FechaHasta", fecha2);
            objReport.setParameters("ProductoCod", productocod);
            objReport.setParameters("EmpresaId", String.valueOf(empresaid));
            objReport.setParameters("BodegaId", BodegaId);
            request.getSession().setAttribute("extension",extension);
            
            if( "xls".equals(extension)){
                objReport.showExcel();
            }else{
                objReport.showReport();
                
            }
            getServletConfig().getServletContext().getRequestDispatcher("/reportview/download.jsp").forward(request,response); 
       /* } */

    } catch (SQLException | ParserConfigurationException | SAXException | ClassNotFoundException | JRException ex) {
        Logger.getLogger(MovimientoProd.class.getName()).log(Level.SEVERE, null, ex);
    }
}
     
     
@Override
public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     
    try {
        int empresaid =  (int) request.getSession().getAttribute("empresaid");
        request.getSession().setAttribute("empresaid", empresaid);
        
        
        ProductoModel objProductoModel = new ProductoModel(empresaid);
        ArrayList<Producto>  arrayproducto = objProductoModel.listProducto(0);
        request.getSession().setAttribute("arrayproducto",arrayproducto);   
       
        BodegaModel objBodegaModel = new BodegaModel(empresaid);
           ArrayList<Bodega> arraylistbodega = objBodegaModel.listallBodegas();
           request.getSession().setAttribute("arraylistbodega", arraylistbodega);
       
     
         request.getSession().setAttribute("modulo", "movimientoprod");
       
        getServletConfig().getServletContext().getRequestDispatcher("/reportview/movproducto.jsp").forward(request,response);
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
        Logger.getLogger(MovimientoProd.class.getName()).log(Level.SEVERE, null, ex);
    }
  
}
    
     
    
}
