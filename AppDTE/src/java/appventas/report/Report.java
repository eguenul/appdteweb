package appventas.report;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import appventas.include.Conexion;
import appventas.include.ConfigAppVenta;
import com.appdte.sii.utilidades.ConfigAppDTE;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import org.xml.sax.SAXException;
public class Report {
Map hm;
String reportName;   
String pathdownload;
String archivosalida;
public Report(String reportName,String pathdownload,String archivosalida,String pathpdf){
    this.reportName = reportName;
    this.pathdownload = pathdownload;
    this.archivosalida = archivosalida;
    hm = new HashMap();
    
}
 
public void  showReport() throws JRException, SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException {
    /* genera el documento en pdf */ 
    ConfigAppVenta objconfig = new ConfigAppVenta();
    ConfigAppDTE objconfigdte = new ConfigAppDTE();
    
         Conexion objConexion = new Conexion();
           objConexion.Conectar();
         
        JasperReport reporte = (JasperReport) JRLoader.loadObjectFromFile(objconfig.getPathreports()+reportName+".jasper");
        
                
        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, hm, objConexion.getConexion());
       
        
        OutputStream output = new FileOutputStream(new File(objconfigdte.getPathpdf()+archivosalida)); 
        JasperExportManager.exportReportToPdfStream(jasperPrint, output);  
        System.out.print(pathdownload+archivosalida);
       objConexion.cerrar();
}
    
    public void setParameters(String parameterName, String parameterValue){
        hm.put(parameterName, parameterValue);
        
    }
    
public void showExcel() throws ParserConfigurationException, SAXException, IOException, SQLException, ClassNotFoundException, JRException{
     ConfigAppVenta objconfig = new ConfigAppVenta();
       Conexion objConexion = new Conexion();
            objConexion.Conectar();
         
     JasperReport reporte = (JasperReport) JRLoader.loadObjectFromFile(objconfig.getPathreports()+reportName+".jasper");
     JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, hm, objConexion.getConexion());
     JRXlsExporter exporter = new JRXlsExporter();
	
     exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
     exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pathdownload+archivosalida));
     SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
     configuration.setOnePagePerSheet(true);
     exporter.setConfiguration(configuration);
     exporter.exportReport();
     objConexion.cerrar();
     /*
     OutputStream output = new FileOutputStream(new File(pathdownload+archivosalida)); 
    JasperExportManager.exportReportToPdfStream(jasperPrint, output);   
     */
}    
    
 
}
