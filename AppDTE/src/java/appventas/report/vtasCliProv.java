/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.report;

import appventas.cliprov.CliProv;
import appventas.cliprov.CliProvModel;
import appventas.include.ConfigAppVenta;
import com.appdte.sii.utilidades.ConfigAppDTE;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import net.sf.jasperreports.engine.JRException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class vtasCliProv extends HttpServlet {
     
@Override
public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
    try {
      CliProv objcliprov = new CliProv();
      objcliprov.setCliprovciu("");
      objcliprov.setCliprovcod(0);
      objcliprov.setCliprovcom("");
      objcliprov.setCliprovdir("");
      objcliprov.setCliprovema("");
      objcliprov.setCliprovfon("");
      objcliprov.setCliprovgir("");
      objcliprov.setCliprovraz("");
      objcliprov.setCliprovrut("");
           
      request.getSession().setAttribute("objcliprov",objcliprov);
        
        
      request.getSession().setAttribute("modulo", "movimiento");
              
        
        
        int empresaid =  (int) request.getSession().getAttribute("empresaid");
        request.getSession().setAttribute("empresaid", empresaid);
        
        CliProvModel objCliProvModel = new CliProvModel(empresaid);
        ArrayList<CliProv> arraylistcliprov = objCliProvModel.listaCliProv(0);
          request.getSession().setAttribute("arraylistcliprov",arraylistcliprov);
        
        getServletConfig().getServletContext().getRequestDispatcher("/reportview/vtacliprov.jsp").forward(request,response);
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
        Logger.getLogger(vtasCliProv.class.getName()).log(Level.SEVERE, null, ex);
    }
  

}
    
@Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
    try {
        int empresaid =  (int) request.getSession().getAttribute("empresaid");
        request.getSession().setAttribute("empresaid", empresaid);
        
           
        request.getSession().setAttribute("modulo", "movimiento");
        CliProvModel objCliProvModel = new CliProvModel(empresaid);      
         ArrayList<CliProv> arraylistcliprov = objCliProvModel.listaCliProv(0);
        request.getSession().setAttribute("arraylistcliprov",arraylistcliprov);
      
        String acc = request.getParameter("ACC");
    
        
        switch(acc){
            
            case "BUSCAR":
                           CliProv objCliProv = objCliProvModel.searchCliProv(Integer.parseInt(request.getParameter("CliProvCod")));
                           request.getSession().setAttribute("objcliprov",objCliProv);
                  
                          getServletConfig().getServletContext().getRequestDispatcher("/reportview/vtacliprov.jsp").forward(request,response);
    
                          break;
       
          case "REPORTE":
                            
              
                        String fechadesde = request.getParameter("FechaDesde");
                        String fechahasta = request.getParameter("FechaHasta");
                        String extension = request.getParameter("TipoReporte");
                        String cliprovcod = request.getParameter("CliProvCod");
                        System.out.print(fechadesde);
              
              
                         String fecha1 = fechadesde;
            
            

                        String fecha2 = fechahasta;

                        ConfigAppVenta objconfig = new ConfigAppVenta();
                        ConfigAppDTE objconfigdte = new ConfigAppDTE();
                        Report objReport = new Report("ventasCLIENTES",objconfig.getPathdownload(),"ventasCLIENTES"+String.valueOf(empresaid) +"."+extension, objconfigdte.getPathpdf());


                        request.getSession().setAttribute("nombredocumento", "ventasCLIENTES"+String.valueOf(empresaid));
                        objReport.setParameters("FechaDesde", fecha1);
                        objReport.setParameters("FechaHasta", fecha2);
                        objReport.setParameters("CliProvCod",  cliprovcod);
                        objReport.setParameters("EmpresaId", String.valueOf(empresaid));
                        request.getSession().setAttribute("extension",extension);

            if( "xls".equals(extension)){
                objReport.showExcel();
            }else{
                objReport.showReport();
                
            }
            getServletConfig().getServletContext().getRequestDispatcher("/reportview/download.jsp").forward(request,response); 
       
              
    
                        break;
       
        
        }
        
        
        
    
    
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | JRException ex) {
        Logger.getLogger(vtasCliProv.class.getName()).log(Level.SEVERE, null, ex);
    }

}
    
    
    
}
