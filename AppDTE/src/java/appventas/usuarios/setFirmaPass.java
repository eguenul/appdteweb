/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.usuarios;

import appventas.include.Funciones;
import com.appdte.sii.utilidades.ConfigAppDTE;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.sql.SQLException;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class setFirmaPass extends HttpServlet{
@Override
public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                  getServletConfig().getServletContext().getRequestDispatcher("/usuarioview/setpassfirma.jsp").forward(request,response);
              
        
}

@Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
 try{      
     
     
    Funciones objFunciones = new Funciones();
   
      
    String login = (String) request.getSession().getAttribute("login");
    objFunciones.loadCert(login);
     
    
    String password = request.getParameter("ClaveFirma");
    ConfigAppDTE objConfigAppDTE = new ConfigAppDTE();     
    KeyStore p12 = KeyStore.getInstance("pkcs12");
    p12.load(new FileInputStream(objConfigAppDTE.getPathcert()+login+".pfx"), password.trim().toCharArray());
    Enumeration e = p12.aliases();
    String alias = (String) e.nextElement();
       
    System.out.println("Alias certifikata:" + alias);
    System.out.print("clave ok");
     
   
         objFunciones.setClaveFirma(login, password);
   
    response.sendRedirect("messageview/passfirmaok.html");
     
    }catch(IOException | ClassNotFoundException | KeyStoreException | NoSuchAlgorithmException | CertificateException | SQLException | ParserConfigurationException | SAXException ex){
     System.out.print("error de clave");
       
        response.sendRedirect("messageview/passfirmaerror.html");
    
          
    } 
      
}    
    
    
    
    
    
    
    
    
}
