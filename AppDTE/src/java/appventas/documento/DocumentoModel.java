
package appventas.documento;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import appventas.include.Conexion;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
public class DocumentoModel {

 


    
public List<Documento> listDocuments() throws SQLException{

    try {
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        
        
        
        
        String sql = "Select * from TipoDocumentos where DTE=1";
        Statement stm = objConexion.getConexion().createStatement();
        ResultSet objrecordset = stm.executeQuery(sql);
        
        List<Documento> arrayDocumento = new ArrayList<>();
        
        
        while(objrecordset.next()){
            Documento objDocumento = new Documento();
            objDocumento.setCodsii(objrecordset.getInt("CodigoSii"));
            objDocumento.setNombredoc(objrecordset.getString("TipoDocumentoDes"));
            objDocumento.setIddoc(objrecordset.getInt("TipoDocumentoId"));
            
            arrayDocumento.add(objDocumento);
            
        }
        objConexion.cerrar();
        return arrayDocumento;
    } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(DocumentoModel.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
}  
    
 
        /*
public int getIddocumento(String nombredoc) throws SQLException{
    String sql = "Select *  from TipoDocumentos where TipoDocumentoDes='"+nombredoc+"'";     
    Statement stm = objconexion.createStatement();
    ResultSet objrecordset = stm.executeQuery(sql);
     objrecordset.next();   
    return objrecordset.getInt("TipoDocumentoId");
}        
*/



public int getIddocumento2(int codsii) throws SQLException{
    try {
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        
        int iddocumento;
        
        String sql = "Select *  from TipoDocumentos where CodigoSii="+String.valueOf(codsii);
        Statement stm = objConexion.getConexion().createStatement();
        ResultSet objrecordset = stm.executeQuery(sql);
        objrecordset.next();
       
        iddocumento = objrecordset.getInt("TipoDocumentoId");
         objConexion.cerrar();
         return iddocumento;
    } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(DocumentoModel.class.getName()).log(Level.SEVERE, null, ex);
    }
    return 0;
    
}       


        
public int getSiiCod(int iddocumento) throws SQLException{
    try {
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        
        
        String sql = "Select *  from TipoDocumentos where TipoDocumentoId="+iddocumento;
        Statement stm = objConexion.getConexion().createStatement();
        ResultSet objrecordset = stm.executeQuery(sql);
        objrecordset.next();
        
        int codsii = objrecordset.getInt("CodigoSii");
        objConexion.cerrar();
        return codsii;
    } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(DocumentoModel.class.getName()).log(Level.SEVERE, null, ex);
    }
    return 0;
}        




public String getNombreDoc(int iddocumento) throws SQLException{
    try {
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        
        
        String sql = "Select *  from TipoDocumentos where TipoDocumentoId="+iddocumento;
        Statement stm = objConexion.getConexion().createStatement();
        ResultSet objrecordset = stm.executeQuery(sql);
        objrecordset.next();
        
        String nombredoc = objrecordset.getString("TipoDocumentoDes");
        objConexion.cerrar();
        return nombredoc;
    } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(DocumentoModel.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
}        



public int searchCorrelativo(int empresaid,String nombredoc) throws SQLException{
    try {
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        
        int numfolio;
        String sql = "Select "+nombredoc+" from Correlativo where EmpresaId="+String.valueOf(empresaid);
        Statement stm = objConexion.getConexion().createStatement();
        ResultSet objrecordset = stm.executeQuery(sql);
        objrecordset.next();
        numfolio = objrecordset.getInt(nombredoc);
        objConexion.cerrar();
        return numfolio;
    } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(DocumentoModel.class.getName()).log(Level.SEVERE, null, ex);
    }
    return 0;
}



public void updateCorrelativo(int empresaid,String nombredoc) throws SQLException{
    try {
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        
        String sql = "Update Correlativo SET "+nombredoc+"="+nombredoc+ "+1 where EmpresaId="+String.valueOf(empresaid);
        Statement stm = objConexion.getConexion().createStatement();
        stm.executeUpdate(sql);
        objConexion.cerrar();
    } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(DocumentoModel.class.getName()).log(Level.SEVERE, null, ex);
    }
}




public int getId(String codsii) throws SQLException{
    try {
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        
        
        int tipodocumentoid = 0;
        String sql = "Select * from TipoDocumentos where CodigoSii="+codsii;
        Statement stm = objConexion.getConexion().createStatement();
        ResultSet objrecordset = stm.executeQuery(sql);
        while (objrecordset.next()){
            tipodocumentoid = objrecordset.getInt("TipoDocumentoId");
        }
        objConexion.cerrar();
        return tipodocumentoid;
    } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(DocumentoModel.class.getName()).log(Level.SEVERE, null, ex);
    }
    return 0;
    
}


public String getNombreDocCodSii(String codsii) throws SQLException{
    try {
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        
        String sql = "Select * from TipoDocumentos where CodigoSii="+codsii;
        Statement stm = objConexion.getConexion().createStatement();
        ResultSet objrecordset = stm.executeQuery(sql);
        objrecordset.next();
        String nombredoc = objrecordset.getString("TipoDocumentoDes");
        objConexion.cerrar();
        return nombredoc;
    } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(DocumentoModel.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
 
   
}

public int getIdNotacion(String notacion) throws SQLException{
    try {
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        
        
        int tipodocumentoid = 0;
        String sql = "Select * from TipoDocumentos where NOTACION='"+notacion+"'";
        Statement stm = objConexion.getConexion().createStatement();
        ResultSet objrecordset = stm.executeQuery(sql);
        while (objrecordset.next()){
            tipodocumentoid = objrecordset.getInt("TipoDocumentoId");
        }
        objConexion.cerrar();
        return tipodocumentoid;
    } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(DocumentoModel.class.getName()).log(Level.SEVERE, null, ex);
    }
    return 0;
    
}

}
