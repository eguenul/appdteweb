/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appventas.login;

import appventas.include.Conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class LoginModel {
    
    public LoginModel() {
          
    }
    
   public boolean authLogin(String loginname,String password) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            
            
            boolean authlogin = false;
            String sql;
            Statement stmt = objConexion.getConexion().createStatement();
            sql = "Select * from Usuario where UsuarioLogin='"+loginname+"' and UsuarioPass='"+password+"'";
            
            ResultSet objrecordset = stmt.executeQuery(sql);
            
            if(objrecordset.next()==true){
                authlogin=true;
            }
            objConexion.cerrar();
            return authlogin;
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(LoginModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
   }
   
   public String getRut(String loginname) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            String sql;
            Statement stmt = objConexion.getConexion().createStatement();
            sql = "Select * from Usuario where UsuarioLogin='"+loginname+"'";
            String rutusuario  = "";
            ResultSet objrecordset = stmt.executeQuery(sql);
            
            if(objrecordset.next()==true){
                rutusuario = objrecordset.getString("UsuarioRut");
            }
            objConexion.cerrar();
            return rutusuario;
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(LoginModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
   
   }
    
}
