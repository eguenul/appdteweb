/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.bodega;

import appventas.include.Conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class BodegaModel {
 
 private final  int empresaid; 
public BodegaModel(int empresaid) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
    this.empresaid = empresaid;
     
}    
    
   public void addBodega(Bodega objBodega) throws SQLException{
     try {
         
         
         
         
         int bodegacod = getCorrelativo(this.empresaid);
         Conexion objConexion = new Conexion();
         objConexion.Conectar();
         String sql = "INSERT INTO Bodega (BodegaCod,BodegaNom,EmpresaId) values(" + String.valueOf(bodegacod)+",'"+objBodega.getBodeganom()+"',"+String.valueOf(this.empresaid)+")";
  
         Statement smt = objConexion.getConexion().createStatement();
         smt.execute(sql);
         objConexion.cerrar();
         updateCorrelativo(this.empresaid);
     } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
         Logger.getLogger(BodegaModel.class.getName()).log(Level.SEVERE, null, ex);
     }
  
   } 
   
   public int getCorrelativo(int empresaid){
     try {
         int bodegacod = 0;
         Conexion objConexion = new Conexion();
         objConexion.Conectar();
         Statement smt1 = objConexion.getConexion().createStatement();
         ResultSet objrecordset = smt1.executeQuery("Select * from Correlativo where EmpresaId=" + String.valueOf(empresaid));
         
         while(objrecordset.next()){
             bodegacod = objrecordset.getInt("BodegaCod");
         }
         objConexion.cerrar();
         return bodegacod;
     } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
         Logger.getLogger(BodegaModel.class.getName()).log(Level.SEVERE, null, ex);
     }
     return 0;
   }
   
   
   public void updateCorrelativo(int empresaid){
     try {
        
         Conexion objConexion = new Conexion();
         objConexion.Conectar();
         Statement smt1 = objConexion.getConexion().createStatement();
         smt1.execute("Update Correlativo SET BodegaCod=BodegaCod+1 where EmpresaId=" + String.valueOf(empresaid));
        
         
         objConexion.cerrar();
            } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
         Logger.getLogger(BodegaModel.class.getName()).log(Level.SEVERE, null, ex);
     }
   }
   
   
   
   
   
   
   
   
   
   
   public ArrayList<Bodega> listBodega() throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
         
       
       
        ArrayList<Bodega> arraylistbodega;
        arraylistbodega = new ArrayList<>();
       
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        Statement smt1 = objConexion.getConexion().createStatement();
        ResultSet objrecordset = smt1.executeQuery("Select * from Bodega where EmpresaId=" + String.valueOf(empresaid) + " LIMIT 0,5");  
        
        while(objrecordset.next()){
          Bodega objBodega = new Bodega();
          objBodega.setBodegacod(objrecordset.getInt("BodegaCod"));
          objBodega.setBodeganom(objrecordset.getString("BodegaNom"));
          arraylistbodega.add(objBodega);
         }
        
        objConexion.cerrar();
        return arraylistbodega;
       
       
   }  
    public ArrayList<Bodega> listBodegaNom(String bodeganom) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
         
       
       
        ArrayList<Bodega> arraylistbodega;
        arraylistbodega = new ArrayList<>();
       
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        Statement smt1 = objConexion.getConexion().createStatement();
        
        String sql = "Select * from Bodega where EmpresaId=" + String.valueOf(empresaid) + " and BodegaNom LIKE '"+ bodeganom+"%'";
        System.out.print(sql);
        ResultSet objrecordset = smt1.executeQuery(sql);  
        
        while(objrecordset.next()){
          Bodega objBodega = new Bodega();
          objBodega.setBodegacod(objrecordset.getInt("BodegaCod"));
          objBodega.setBodeganom(objrecordset.getString("BodegaNom"));
          arraylistbodega.add(objBodega);
         }
        
        objConexion.cerrar();
        return arraylistbodega;
       
       
   }


  public ArrayList<Bodega> listBodegaCod(int bodegacod) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
         
       
       
        ArrayList<Bodega> arraylistbodega;
        arraylistbodega = new ArrayList<>();
       
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        Statement smt1 = objConexion.getConexion().createStatement();
        
        String sql = "Select * from Bodega where EmpresaId=" + String.valueOf(empresaid) + " and BodegaCod="+ String.valueOf(bodegacod);
        System.out.print(sql);
        ResultSet objrecordset = smt1.executeQuery(sql);  
        
        while(objrecordset.next()){
          Bodega objBodega = new Bodega();
          objBodega.setBodegacod(objrecordset.getInt("BodegaCod"));
          objBodega.setBodeganom(objrecordset.getString("BodegaNom"));
          arraylistbodega.add(objBodega);
         }
        
        objConexion.cerrar();
        return arraylistbodega;
       
       
   }    


 public ArrayList<Bodega> listallBodegas () throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
         
       
       
        ArrayList<Bodega> arraylistbodega;
        arraylistbodega = new ArrayList<>();
       
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        Statement smt1 = objConexion.getConexion().createStatement();
        
        String sql = "Select * from Bodega where EmpresaId=" + String.valueOf(this.empresaid);
        System.out.print(sql);
        ResultSet objrecordset = smt1.executeQuery(sql);  
        
        while(objrecordset.next()){
          Bodega objBodega = new Bodega();
          objBodega.setBodegacod(objrecordset.getInt("BodegaCod"));
          objBodega.setBodeganom(objrecordset.getString("BodegaNom"));
          objBodega.setBodegaid(objrecordset.getInt("BodegaId"));
          arraylistbodega.add(objBodega);
         }
        
        objConexion.cerrar();
        return arraylistbodega;
       
       
   }    





    
     
}
