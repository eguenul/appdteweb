/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.bodega;

/**
 *
 * @author esteban
 */
public class Bodega {
private int empresaid;
private int bodegacod;
private int bodegaid;
private String bodeganom;

    public int getEmpresaid() {
        return empresaid;
    }

    public void setEmpresaid(int empresaid) {
        this.empresaid = empresaid;
    }

    public int getBodegacod() {
        return bodegacod;
    }

    public void setBodegacod(int bodegacod) {
        this.bodegacod = bodegacod;
    }

    public String getBodeganom() {
        return bodeganom;
    }

    public void setBodeganom(String bodeganom) {
        this.bodeganom = bodeganom;
    }

    public int getBodegaid() {
        return bodegaid;
    }

    public void setBodegaid(int bodegaid) {
        this.bodegaid = bodegaid;
    }



}
