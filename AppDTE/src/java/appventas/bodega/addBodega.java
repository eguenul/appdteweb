/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.bodega;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class addBodega extends HttpServlet {
    
    
@Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
    try {
        int empresaid =  (int) request.getSession().getAttribute("empresaid");
        request.getSession().setAttribute("empresaid", empresaid);
        Bodega objBodega = new Bodega();
        BodegaModel objBodegaModel = new BodegaModel(empresaid);
        objBodega.setBodegacod(0);
        objBodega.setBodeganom(request.getParameter("BodegaNom"));
        objBodegaModel.addBodega(objBodega);
        getServletConfig().getServletContext().getRequestDispatcher("/bodegaview/formbodega.jsp").forward(request,response);
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
        Logger.getLogger(addBodega.class.getName()).log(Level.SEVERE, null, ex);
    }
}    


@Override
public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
    try {
        int empresaid =  (int) request.getSession().getAttribute("empresaid");
         BodegaModel objBodegaModel = new BodegaModel(empresaid);
        ArrayList<Bodega> arraylistbodega = objBodegaModel.listBodega();
        
              
      request.getSession().setAttribute("arraylistbodega",arraylistbodega);
        
        getServletConfig().getServletContext().getRequestDispatcher("/bodegaview/bodega.jsp").forward(request,response);
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
        Logger.getLogger(addBodega.class.getName()).log(Level.SEVERE, null, ex);
    }
   

}  


}
