/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.admincaf;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class adminCAF extends HttpServlet {
    
  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  
      try {
          int empresaid =  (int) request.getSession().getAttribute("empresaid");
          
          listCAF objlistCAF = new listCAF();
          
        ArrayList<Object[]> arraylistcaf = objlistCAF.listCAF(empresaid);
          request.getSession().setAttribute("arraylistcaf", arraylistcaf);
          
          
          getServletConfig().getServletContext().getRequestDispatcher("/admincafview/adminCAF.jsp").forward(request,response);
      } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
          Logger.getLogger(adminCAF.class.getName()).log(Level.SEVERE, null, ex);
      }
  
  }   
    
  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  

  }   
    
    
}
