/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.admincaf;

import appventas.include.Conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class listCAF {
    
    public ArrayList<Object[]> listCAF(int empresaid) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
         Conexion objConexion = new Conexion();
        objConexion.Conectar();
        
        
         String sql ="Select CAF.idCAF, CAF.Desde, CAF.Hasta, TipoDocumentos.TipoDocumentoDes from CAF \n" +
        "INNER join TipoDocumentos on CAF.TipoCaf = TipoDocumentos.TipoDocumentoId where CAF.EmpresaId="+String.valueOf(empresaid); 
            
        Statement stm = objConexion.getConexion().createStatement();
        ResultSet objrecordset = stm.executeQuery(sql);
      
        
         ArrayList<Object[]> arraylistcaf = new ArrayList<>();
        
         
   while (objrecordset.next()){
         Object[] auxData = new Object[4];
       auxData[0] = objrecordset.getInt("idCAF");
       auxData[1] = objrecordset.getInt("Desde");
       auxData[2] = objrecordset.getInt("Hasta");
       auxData[3] = objrecordset.getString("TipoDocumentoDes");
       arraylistcaf.add(auxData);
        }
        objConexion.cerrar();
        return arraylistcaf;
    }
    
    
    
    
    
}
