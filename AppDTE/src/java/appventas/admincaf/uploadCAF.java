/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.admincaf;


import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import com.appdte.sii.utilidades.ConfigAppDTE;
import java.sql.SQLException;


import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;


public class uploadCAF extends HttpServlet {
   
  
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            
            String login=  (String) request.getSession().getAttribute("login");
            
            ConfigAppDTE objconfig = new ConfigAppDTE();
             String  pathCAF = objconfig.getPathcaf();
             String name="";
            if(ServletFileUpload.isMultipartContent(request)){
                try {
                    List<FileItem> multiparts = new ServletFileUpload(
                            new DiskFileItemFactory()).parseRequest(request);
                    
                    for(FileItem item : multiparts){
                        if(!item.isFormField()){
                            name = new File(item.getName()).getName();
                            item.write( new File(pathCAF+login + name));
                             
                            System.out.print(name);
                        }
                    }
                    
                    //File uploaded successfully
                      System.out.print("CAF CORRECTAMENTE CARGADO");
                } catch (Exception ex) {
                    request.setAttribute("message", "File Upload Failed due to " + ex);
                }
                
            }else{
                request.setAttribute("message","Sorry this Servlet only handles file upload request");
            }
            
           /* AHORA ABRO EL CAF PARA LEERLO Y GUARDARLO COMO ARREGLO DE BYTES */ 
            
            int empresaid = (int) request.getSession().getAttribute("empresaid");
    
            saveCAf objsaveCAf = new saveCAf();
           
           if(objsaveCAf.readCAF(empresaid, pathCAF+login + name)==false){
               response.sendRedirect("messageview/errorfile.html");    
               
           }else{
               objsaveCAf.guardarCAF(empresaid, pathCAF+login + name);               
           }
           
              response.sendRedirect("messageview/cafok.html");
           
           /* FINALIZO LA LECTURA DEL CAF */   
           /*
           request.getSession().setAttribute("pathdte",pathCAF+login+name);
        
            
            */
        } catch ( IllegalStateException | ParserConfigurationException | SAXException | SQLException | ClassNotFoundException ex) {
           
             Logger.getLogger(uploadCAF.class.getName()).log(Level.SEVERE, null, ex);
         
        }
        /*
         */ 
        
        /*
     
        
*/  
     
    }
    
    
    }
    

      
