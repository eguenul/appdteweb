/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.tpoventa;

import appventas.include.Conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class TpoVentaModel {
     
     
     public TpoVentaModel() throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
         
          
     }
    
    public ArrayList<TpoVenta> listTpoVenta() throws SQLException{
        
         try {
             Conexion objConexion = new Conexion();
             objConexion.Conectar();
             
             
             ArrayList<TpoVenta> arraylist1 = new  ArrayList<>();
             String sql = "Select * from TpoVenta";
             Statement stm = objConexion.getConexion().createStatement();
             ResultSet objrecordset = stm.executeQuery(sql);
             
             while(objrecordset.next()){
                 TpoVenta objTpoVenta = new TpoVenta();
                 objTpoVenta.setIdtpoventa(objrecordset.getInt("IdTpoVenta"));
                 objTpoVenta.setCodSii(objrecordset.getString("CodSii"));
                 objTpoVenta.setDescripcion(objrecordset.getString("Descripcion"));
                 arraylist1.add(objTpoVenta);
             }
             objConexion.cerrar();
             return arraylist1;
         } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
             Logger.getLogger(TpoVentaModel.class.getName()).log(Level.SEVERE, null, ex);
         }
         return null;
    }
    
     public int getTpoVentaId(int parmcodsii){
        
         
         try {
             Conexion objConexion = new Conexion();
             objConexion.Conectar();
             
             
             int tpoventaid = 0;
             String sql = "Select * from TpoVenta where CodSii="+ String.valueOf(parmcodsii);
             Statement stm = objConexion.getConexion().createStatement();
             ResultSet objrecordset = stm.executeQuery(sql);
             
             while(objrecordset.next()){
                 tpoventaid = objrecordset.getInt("IdTpoVenta");
                 
             }
             objConexion.cerrar();
             return tpoventaid;
         } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
             Logger.getLogger(TpoVentaModel.class.getName()).log(Level.SEVERE, null, ex);
         }
         return 0;
         
    }
    
    
    
    
    
}
