/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.consultassii;

import com.appboleta.json.stateJson;
import com.appboleta.sii.TokenBOLETA;
import com.appboleta.sii.bolSTATE;
import com.appboleta.sii.seedBOLETA;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class ConsultaBOLETA  {
    
        
      
private String RutConsultante; 
private String DvConsultante; 
private String RutCompania;
private String DvCompania;         
private String RutReceptor;       
private String DvReceptor;
private String TipoDte;
private String FolioDte;
private String FechaEmisionDte;
private String MontoDte;       
           


public String getEstDte( String login, String clave) throws ParserConfigurationException, SAXException, IOException, Exception{
    seedBOLETA  objSemilla = new seedBOLETA();
    TokenBOLETA objToken = new TokenBOLETA();
    String valorsemilla = objSemilla.getSeed();
    String  stringToken = objToken.getToken(valorsemilla, login, clave);
    
    String estadoboleta = getEstadoEnvio(stringToken);
    
     InputStream isjson = new ByteArrayInputStream(estadoboleta.getBytes("UTF-8")); 
    BufferedReader br1 = new BufferedReader(new InputStreamReader(isjson));
  
  
    Gson gson = new Gson(); 
    stateJson objestadoboleta = gson.fromJson(br1, stateJson.class);
 
    return objestadoboleta.getCodigo();
    
    
    }


public String getEstadoEnvio(String valortoken) throws IOException{
    
      try {
          
          StringBuilder result = new StringBuilder();
          String urlconsulta = "https://apicert.sii.cl/recursos/v1/boleta.electronica/"+getRutCompania()+"-"+getDvCompania()+"-"+getTipoDte()+"-"+getFolioDte()+"/estado?rut_receptor="+getRutReceptor()+"&dv_receptor="+ getDvReceptor()+"&monto="+ getMontoDte()+"&fechaEmision="+getFechaEmisionDte();
          System.out.print(urlconsulta);
          URL url = new URL(urlconsulta);
          HttpURLConnection conn = (HttpURLConnection) url.openConnection();
          conn.setRequestMethod("GET");
          
           conn.setRequestProperty("Cookie","TOKEN="+valortoken);
           /*
           conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=9022632e1130lc4"); 
   */
       try (BufferedReader reader = new BufferedReader(
                  new InputStreamReader(conn.getInputStream()))) {
              for (String line; (line = reader.readLine()) != null; ) {
                  result.append(line);
              }
              
              System.out.print(result.toString());
              return result.toString();
          }
          
          
          
      }   catch (MalformedURLException ex) {
        Logger.getLogger(bolSTATE.class.getName()).log(Level.SEVERE, null, ex);
        return null;
      }

}






    public String getRutConsultante() {
        return RutConsultante;
    }

    public void setRutConsultante(String RutConsultante) {
        this.RutConsultante = RutConsultante;
    }

    public String getDvConsultante() {
        return DvConsultante;
    }

    public void setDvConsultante(String DvConsultante) {
        this.DvConsultante = DvConsultante;
    }

    public String getRutCompania() {
        return RutCompania;
    }

    public void setRutCompania(String RutCompania) {
        this.RutCompania = RutCompania;
    }

    public String getDvCompania() {
        return DvCompania;
    }

    public void setDvCompania(String DvCompania) {
        this.DvCompania = DvCompania;
    }

    public String getRutReceptor() {
        return RutReceptor;
    }

    public void setRutReceptor(String RutReceptor) {
        this.RutReceptor = RutReceptor;
    }

    public String getDvReceptor() {
        return DvReceptor;
    }

    public void setDvReceptor(String DvReceptor) {
        this.DvReceptor = DvReceptor;
    }

    public String getTipoDte() {
        return TipoDte;
    }

    public void setTipoDte(String TipoDte) {
        this.TipoDte = TipoDte;
    }

    public String getFolioDte() {
        return FolioDte;
    }

    public void setFolioDte(String FolioDte) {
        this.FolioDte = FolioDte;
    }

    public String getFechaEmisionDte() {
        return FechaEmisionDte;
    }

    public void setFechaEmisionDte(String FechaEmisionDte) {
        this.FechaEmisionDte = FechaEmisionDte;
    }

    public String getMontoDte() {
        return MontoDte;
    }

    public void setMontoDte(String MontoDte) {
        this.MontoDte = MontoDte;
    }
    
    
    
}


   

