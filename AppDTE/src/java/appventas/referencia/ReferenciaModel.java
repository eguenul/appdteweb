/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.referencia;

import appventas.include.Conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class ReferenciaModel {
    public ArrayList<Referencia> listTpoReferencia() throws SQLException, ParserConfigurationException, SAXException, IOException{
        
         try {
             Conexion objConexion = new Conexion();
             objConexion.Conectar();
             
             
             ArrayList<Referencia> arraylist1 = new  ArrayList<>();
             String sql = "Select * from Referencia order by ReferenciaCod ASC";
             Statement stm = objConexion.getConexion().createStatement();
             ResultSet objrecordset = stm.executeQuery(sql);
             
             while(objrecordset.next()){
                 Referencia objReferencia = new Referencia();
                 objReferencia.setReferenciaid(objrecordset.getInt("Id"));
                 objReferencia.setReferenciacod(objrecordset.getInt("ReferenciaCod"));
                 objReferencia.setReferenciades(objrecordset.getString("ReferenciaDes"));
                 arraylist1.add(objReferencia);
             }
             objConexion.cerrar();
             return arraylist1;
         } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
             Logger.getLogger(ReferenciaModel.class.getName()).log(Level.SEVERE, null, ex);
         }
         return null;
    }
    
}
