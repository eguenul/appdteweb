/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appventas.referencia;

/**
 *
 * @author esteban
 */
public class Referencia {
   private int referenciaid;
   private int referenciacod;
   private String referenciades;

    public int getReferenciaid() {
        return referenciaid;
    }

    public void setReferenciaid(int referenciaid) {
        this.referenciaid = referenciaid;
    }

    public int getReferenciacod() {
        return referenciacod;
    }

    public void setReferenciacod(int referenciacod) {
        this.referenciacod = referenciacod;
    }

    public String getReferenciades() {
        return referenciades;
    }

    public void setReferenciades(String referenciades) {
        this.referenciades = referenciades;
    }
   
   
   
   
}
