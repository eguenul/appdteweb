/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.appdte.models;

/**
 *
 * @author esteban
 */
public class ImptoReten {

    private int tipoimp;
    private int tasaimp;
    private int montoimp;

    public int getTipoimp() {
        return tipoimp;
    }

    public void setTipoimp(int tipoimp) {
        this.tipoimp = tipoimp;
    }

    public int getTasaimp() {
        return tasaimp;
    }

    public void setTasaimp(int tasaimp) {
        this.tasaimp = tasaimp;
    }

    public int getMontoimp() {
        return montoimp;
    }

    public void setMontoimp(int montoimp) {
        this.montoimp = montoimp;
    }



    
}
